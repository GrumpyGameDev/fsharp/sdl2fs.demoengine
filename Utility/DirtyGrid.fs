namespace Utility

type DirtyGrid<'TValue> =
    {
        committed: Map<int * int,'TValue>
        pending: Map<int * int,'TValue option>
    }

    member x.Commit() : DirtyGrid<'TValue> =
        { 
            committed =
                x.pending
                |> Map.fold
                    (fun a k v -> 
                        match v with
                        | Some h ->
                            a
                            |> Map.add k h
                        | None ->
                            a
                            |> Map.remove k) x.committed
            pending = Map.empty
        }

    member x.Set (location:int * int) (value:'TValue) : DirtyGrid<'TValue> =
        {x with pending = x.pending |> Map.add location (value |> Some)}

    member x.Reset (location:int * int) : DirtyGrid<'TValue> =
        {x with pending = x.pending |> Map.add location None}

    member x.Iter (iterator:int * int->'TValue->unit) : unit=
        x.committed
        |> Map.iter iterator

    member x.Fold (foldFunc:'TAccumulator->(int * int)->'TValue->'TAccumulator) (accumulator:'TAccumulator) =
        x.committed
        |> Map.fold foldFunc accumulator

module DirtyGrid =
    let create() : DirtyGrid<'TValue> =
        {
            committed=Map.empty
            pending=Map.empty
        }
        
    let commit (grid:DirtyGrid<'TValue>) : DirtyGrid<'TValue> =
        grid.Commit()

    let tryGet (location:int*int) (grid:DirtyGrid<'TValue>) : 'TValue option =
        grid.committed
        |> Map.tryFind location

    let set (location:int * int) (value:'TValue) (grid:DirtyGrid<'TValue>) : DirtyGrid<'TValue> =
        grid.Set location value

    let reset (location:int * int) (grid:DirtyGrid<'TValue>) : DirtyGrid<'TValue> =
        grid.Reset location

    let put (location:int * int) = 
        function
        | Some v -> set location v
        | None -> reset location

    let putRect (location:int * int) (size:int*int) (value:'TValue option) (grid:DirtyGrid<'TValue>) : DirtyGrid<'TValue> =
        let xValues = 
            [(location |> fst)..((location |> fst) + (size |> fst) - 1)]
        let yValues = 
            [(location |> snd)..((location |> snd) + (size |> snd) - 1)]
        xValues
        |> List.fold
            (fun g x ->
                yValues
                |> List.fold
                    (fun g' y -> 
                        g'
                        |> put (x,y) value) g) grid

    let iter (iterator:int * int->'TValue->unit) (grid:DirtyGrid<'TValue>): unit =
        grid.Iter iterator

    let fold (foldFunc:'TAccumulator->(int * int)->'TValue->'TAccumulator) (accumulator:'TAccumulator) (grid:DirtyGrid<'TValue>) : 'TAccumulator =
        grid.Fold foldFunc accumulator

    let ofStringList (transform:char -> ('TValue option)) (strings:string list) : DirtyGrid<'TValue> =
        ((0,create()), strings)
        ||> List.fold 
            (fun (y,grid) s -> 
                let newGrid =
                    ((0, grid), s.ToCharArray())
                    ||> Array.fold
                        (fun (x,g) c ->
                            (x+1, g |> put (x,y) (c |> transform)))
                    |> snd
                (y+1,newGrid))
        |> snd
        |> commit
        
    let ofByteList (transform:bool -> ('TValue option)) (bytes:byte list) : DirtyGrid<'TValue> =
        let flags = [|1uy;2uy;4uy;8uy;16uy;32uy;64uy;128uy|]
        ((0,create()), bytes)
        ||> List.fold 
            (fun (y,grid) v -> 
                let newGrid =
                    ((0, grid), flags)
                    ||> Array.fold
                        (fun (x,g) f ->
                            (x+1, g |> put (x,y) (((f &&& v)>0uy) |> transform)))
                    |> snd
                (y+1,newGrid))
        |> snd
        |> commit

    let write (transform:'TOther -> ('TValue option)) (otherGrid:DirtyGrid<'TOther>) (grid:DirtyGrid<'TValue>): DirtyGrid<'TValue> =
        otherGrid.Commit().committed
        |> Map.fold
            (fun a k v -> 
                a
                |> put k (v |> transform)) grid

    let writeCommit (transform:'TOther -> ('TValue option)) (otherGrid:DirtyGrid<'TOther>) =
        write transform otherGrid >> commit

    let translate (translator:int * int->(int * int) list) (grid:DirtyGrid<'TValue>) : DirtyGrid<'TValue> =
        grid.committed
        |> Map.fold
            (fun g k v -> 
                k
                |> translator
                |> List.fold
                    (fun g' i -> 
                        g'
                        |> set i v) g) (create())

    let translateCommit (translator:int * int->(int * int) list) =
        translate translator >> commit

    let tint (tinter:'TOther->'TValue) (grid:DirtyGrid<'TOther>) : DirtyGrid<'TValue> =
        grid.committed
        |> Map.fold
            (fun g k v -> 
                g
                |> set k (v |> tinter)) (create())
    
    let tintCommit (tinter:'TOther->'TValue) =
        tint tinter >> commit

    let updateCell (updater:'TValue option->'TValue option) (location:int * int) (grid:DirtyGrid<'TValue>) : DirtyGrid<'TValue> =
        grid
        |> put location (grid.committed |> Map.tryFind location |> updater)

    let updateCellCommit (updater:'TValue option->'TValue option) (location:int * int) =
        updateCell updater location >> commit

    let mapCell (updater:'TValue->'TValue) (location:int * int) (grid:DirtyGrid<'TValue>) : DirtyGrid<'TValue> =
        grid
        |> updateCell (Option.map updater) location

    let mapCellCommit (updater:'TValue->'TValue) (location:int * int) =
        mapCell updater location >> commit
