namespace Utility

type EventResult<'TData,'TContext> =
    | Continue of State<'TData,'TContext>
    | Stop of 'TContext

type EventSource<'TEvent> = unit -> 'TEvent option
type EventHandler<'TEvent,'TData, 'TContext> = 'TEvent -> State<'TData,'TContext> -> EventResult<'TData,'TContext>
type IdleHandler<'TData, 'TContext> = State<'TData,'TContext> -> State<'TData,'TContext>
type PresentationHandler<'TData,'TContext> = State<'TData,'TContext> -> unit

module EventPump = 

    let rec start 
            (eventSource: EventSource<'TEvent>) 
            (eventHandler: EventHandler<'TEvent, 'TData, 'TContext>) 
            (idleHandler: IdleHandler<'TData, 'TContext>) 
            (presentationHandler: PresentationHandler<'TData, 'TContext>)
            (state: State<'TData, 'TContext>) :'TContext =

        //present the state
        presentationHandler state

        //check for an event
        match eventSource() with

        //an event occurred
        | Some event ->

            //send event to event handler, and then pump again if a state is returned
            match (event, state) ||> eventHandler with
            | Continue state' -> 
                start eventSource eventHandler idleHandler presentationHandler state' 
            | Stop context-> 
                context

        //no event occurred
        | None ->
            state
            |> idleHandler
            |> start eventSource eventHandler idleHandler presentationHandler
