namespace Utility

module Location =
    let move 
            (delta:int * int) 
            (location:int * int) 
            : int * int =
        ((delta |> fst)+(location|> fst),
            (delta |> snd)+(location |> snd))

    let scale 
            (scale: int * int) 
            (location:int * int) 
            : int * int =        
        ((scale |> fst)*(location|> fst),
            (scale |> snd)*(location |> snd))

    let toList 
            (value:int * int) 
            : (int * int) list =
        [value]
