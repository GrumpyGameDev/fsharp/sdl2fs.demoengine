namespace Utility

type State<'TData,'TContext> = 
    {
        data:'TData
        context:'TContext
    }

    member x.UpdateData
            (updater:'TData -> 'TData) 
            : State<'TData,'TContext> =
        {x with data = x.data |> updater}

    member x.UpdateContext
            (updater:'TContext -> 'TContext) 
            : State<'TData,'TContext> =
        {x with context = x.context |> updater}


module State=

    let create 
            (data:'TData, context:'TContext) 
            : State<'TData,'TContext> =
        {
            data = data
            context=context
        }

    let updateData 
            (updater:'TData -> 'TData) 
            (state:State<'TData,'TContext>) 
            : State<'TData,'TContext> =
        state.UpdateData updater

    let updateContext 
            (updater:'TContext -> 'TContext) 
            (state:State<'TData,'TContext>) 
            : State<'TData,'TContext> =
        state.UpdateContext updater

