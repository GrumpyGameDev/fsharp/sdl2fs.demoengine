namespace Utility

type CharacterPattern =
    | Character00
    | Character01
    | Character02
    | Character03
    | Character04
    | Character05
    | Character06
    | Character07
    | Character08
    | Character09
    | Character0A
    | Character0B
    | Character0C
    | Character0D
    | Character0E
    | Character0F
    | Character10
    | Character11
    | Character12
    | Character13
    | Character14
    | Character15
    | Character16
    | Character17
    | Character18
    | Character19
    | Character1A
    | Character1B
    | Character1C
    | Character1D
    | Character1E
    | Character1F
    | Character20
    | Character21
    | Character22
    | Character23
    | Character24
    | Character25
    | Character26
    | Character27
    | Character28
    | Character29
    | Character2A
    | Character2B
    | Character2C
    | Character2D
    | Character2E
    | Character2F
    | Character30
    | Character31
    | Character32
    | Character33
    | Character34
    | Character35
    | Character36
    | Character37
    | Character38
    | Character39
    | Character3A
    | Character3B
    | Character3C
    | Character3D
    | Character3E
    | Character3F
    | Character40
    | Character41
    | Character42
    | Character43
    | Character44
    | Character45
    | Character46
    | Character47
    | Character48
    | Character49
    | Character4A
    | Character4B
    | Character4C
    | Character4D
    | Character4E
    | Character4F
    | Character50
    | Character51
    | Character52
    | Character53
    | Character54
    | Character55
    | Character56
    | Character57
    | Character58
    | Character59
    | Character5A
    | Character5B
    | Character5C
    | Character5D
    | Character5E
    | Character5F
    | Character60
    | Character61
    | Character62
    | Character63
    | Character64
    | Character65
    | Character66
    | Character67
    | Character68
    | Character69
    | Character6A
    | Character6B
    | Character6C
    | Character6D
    | Character6E
    | Character6F
    | Character70
    | Character71
    | Character72
    | Character73
    | Character74
    | Character75
    | Character76
    | Character77
    | Character78
    | Character79
    | Character7A
    | Character7B
    | Character7C
    | Character7D
    | Character7E
    | Character7F
    | Character80
    | Character81
    | Character82
    | Character83
    | Character84
    | Character85
    | Character86
    | Character87
    | Character88
    | Character89
    | Character8A
    | Character8B
    | Character8C
    | Character8D
    | Character8E
    | Character8F
    | Character90
    | Character91
    | Character92
    | Character93
    | Character94
    | Character95
    | Character96
    | Character97
    | Character98
    | Character99
    | Character9A
    | Character9B
    | Character9C
    | Character9D
    | Character9E
    | Character9F
    | CharacterA0
    | CharacterA1
    | CharacterA2
    | CharacterA3
    | CharacterA4
    | CharacterA5
    | CharacterA6
    | CharacterA7
    | CharacterA8
    | CharacterA9
    | CharacterAA
    | CharacterAB
    | CharacterAC
    | CharacterAD
    | CharacterAE
    | CharacterAF
    | CharacterB0
    | CharacterB1
    | CharacterB2
    | CharacterB3
    | CharacterB4
    | CharacterB5
    | CharacterB6
    | CharacterB7
    | CharacterB8
    | CharacterB9
    | CharacterBA
    | CharacterBB
    | CharacterBC
    | CharacterBD
    | CharacterBE
    | CharacterBF
    | CharacterC0
    | CharacterC1
    | CharacterC2
    | CharacterC3
    | CharacterC4
    | CharacterC5
    | CharacterC6
    | CharacterC7
    | CharacterC8
    | CharacterC9
    | CharacterCA
    | CharacterCB
    | CharacterCC
    | CharacterCD
    | CharacterCE
    | CharacterCF
    | CharacterD0
    | CharacterD1
    | CharacterD2
    | CharacterD3
    | CharacterD4
    | CharacterD5
    | CharacterD6
    | CharacterD7
    | CharacterD8
    | CharacterD9
    | CharacterDA
    | CharacterDB
    | CharacterDC
    | CharacterDD
    | CharacterDE
    | CharacterDF
    | CharacterE0
    | CharacterE1
    | CharacterE2
    | CharacterE3
    | CharacterE4
    | CharacterE5
    | CharacterE6
    | CharacterE7
    | CharacterE8
    | CharacterE9
    | CharacterEA
    | CharacterEB
    | CharacterEC
    | CharacterED
    | CharacterEE
    | CharacterEF
    | CharacterF0
    | CharacterF1
    | CharacterF2
    | CharacterF3
    | CharacterF4
    | CharacterF5
    | CharacterF6
    | CharacterF7
    | CharacterF8
    | CharacterF9
    | CharacterFA
    | CharacterFB
    | CharacterFC
    | CharacterFD
    | CharacterFE
    | CharacterFF
    member x.PixelMap : DirtyGrid<bool> =
        match x with
        | Character00 -> [0x00uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character01 -> [ 126uy;  129uy;  165uy;  129uy;  189uy;  153uy;  129uy;  126uy] |> DirtyGrid.ofByteList Some
        | Character02 -> [ 126uy; 0xFFuy;  219uy; 0xFFuy;  195uy;  231uy; 0xFFuy;  126uy] |> DirtyGrid.ofByteList Some
        | Character03 -> [  54uy;  127uy;  127uy;  127uy;   62uy;   28uy;    8uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character04 -> [   8uy;   28uy;   62uy;  127uy;   62uy;   28uy;    8uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character05 -> [  28uy;   62uy;   28uy;  127uy;  127uy;   73uy;    8uy;   62uy] |> DirtyGrid.ofByteList Some
        | Character06 -> [0x00uy;    8uy;   28uy;   62uy;  127uy;   62uy;   28uy;   62uy] |> DirtyGrid.ofByteList Some
        | Character07 -> [0x00uy; 0x00uy;   24uy;   60uy;   60uy;   24uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character08 -> [0xFFuy; 0xFFuy;  231uy;  195uy;  195uy;  231uy; 0xFFuy; 0xFFuy] |> DirtyGrid.ofByteList Some
        | Character09 -> [0x00uy;   60uy;  102uy;   66uy;   66uy;  102uy;   60uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character0A -> [0xFFuy;  195uy;  153uy;  189uy;  189uy;  153uy;  195uy; 0xFFuy] |> DirtyGrid.ofByteList Some
        | Character0B -> [ 240uy;  224uy;  240uy;  190uy;   51uy;   51uy;   51uy;   30uy] |> DirtyGrid.ofByteList Some
        | Character0C -> [  60uy;  102uy;  102uy;  102uy;   60uy;   24uy;  126uy;   24uy] |> DirtyGrid.ofByteList Some
        | Character0D -> [ 252uy;  204uy;  252uy;   12uy;   12uy;   14uy;   15uy;    7uy] |> DirtyGrid.ofByteList Some
        | Character0E -> [ 254uy;  198uy;  254uy;  198uy;  198uy;  230uy;  103uy;    3uy] |> DirtyGrid.ofByteList Some
        | Character0F -> [ 153uy;   90uy;   60uy;  231uy;  231uy;   60uy;   90uy;  153uy] |> DirtyGrid.ofByteList Some
        | Character10 -> [   1uy;    7uy;   31uy;  127uy;   31uy;    7uy;    1uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character11 -> [  64uy;  112uy;  124uy;  127uy;  124uy;  112uy;   64uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character12 -> [  24uy;   60uy;  126uy;   24uy;   24uy;  126uy;   60uy;   24uy] |> DirtyGrid.ofByteList Some
        | Character13 -> [ 102uy;  102uy;  102uy;  102uy;  102uy; 0x00uy;  102uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character14 -> [ 254uy;  219uy;  219uy;  222uy;  216uy;  216uy;  216uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character15 -> [ 124uy;  198uy;   28uy;   54uy;   54uy;   28uy;   97uy;   63uy] |> DirtyGrid.ofByteList Some
        | Character16 -> [0x00uy; 0x00uy; 0x00uy; 0x00uy;  126uy;  126uy;  126uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character17 -> [  24uy;   60uy;  126uy;   24uy;  126uy;   60uy;   24uy; 0xFFuy] |> DirtyGrid.ofByteList Some
        | Character18 -> [  24uy;   60uy;  126uy;   24uy;   24uy;   24uy;   24uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character19 -> [  24uy;   24uy;   24uy;   24uy;  126uy;   60uy;   24uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character1A -> [0x00uy;   24uy;   48uy;  127uy;   48uy;   24uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character1B -> [0x00uy;   12uy;    6uy;  127uy;    6uy;   12uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character1C -> [0x00uy; 0x00uy;    3uy;    3uy;    3uy;  127uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character1D -> [0x00uy;   36uy;  102uy; 0xFFuy;  102uy;   36uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character1E -> [0x00uy;   24uy;   60uy;  126uy; 0xFFuy; 0xFFuy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character1F -> [0x00uy; 0xFFuy; 0xFFuy;  126uy;   60uy;   24uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character20 -> [0x00uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character21 -> [  24uy;   60uy;   60uy;   24uy;   24uy; 0x00uy;   24uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character22 -> [  54uy;   54uy;   54uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character23 -> [  54uy;   54uy;  127uy;   54uy;  127uy;   54uy;   54uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character24 -> [  24uy;  126uy;    3uy;   62uy;   96uy;   63uy;   24uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character25 -> [0x00uy;   99uy;   51uy;   24uy;   12uy;  102uy;   99uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character26 -> [  28uy;   54uy;   28uy;  110uy;   59uy;   51uy;  110uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character27 -> [  12uy;   12uy;    6uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character28 -> [  24uy;   12uy;    6uy;    6uy;    6uy;   12uy;   24uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character29 -> [   6uy;   12uy;   24uy;   24uy;   24uy;   12uy;    6uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character2A -> [0x00uy;  102uy;   60uy; 0xFFuy;   60uy;  102uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character2B -> [0x00uy;   24uy;   24uy;  126uy;   24uy;   24uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character2C -> [0x00uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy;   24uy;   24uy;   12uy] |> DirtyGrid.ofByteList Some
        | Character2D -> [0x00uy; 0x00uy; 0x00uy;  126uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character2E -> [0x00uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy;   24uy;   24uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character2F -> [  96uy;   48uy;   24uy;   12uy;    6uy;    3uy;    1uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character30 -> [  62uy;   99uy;  107uy;  107uy;  107uy;   99uy;   62uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character31 -> [  12uy;   14uy;   12uy;   12uy;   12uy;   12uy;   63uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character32 -> [  30uy;   51uy;   48uy;   28uy;    6uy;   51uy;   63uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character33 -> [  30uy;   51uy;   48uy;   28uy;   48uy;   51uy;   30uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character34 -> [  56uy;   60uy;   54uy;   51uy;  127uy;   48uy;  120uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character35 -> [  63uy;    3uy;   31uy;   48uy;   48uy;   51uy;   30uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character36 -> [  28uy;    6uy;    3uy;   31uy;   51uy;   51uy;   30uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character37 -> [  63uy;   51uy;   48uy;   24uy;   12uy;   12uy;   12uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character38 -> [  30uy;   51uy;   51uy;   30uy;   51uy;   51uy;   30uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character39 -> [  30uy;   51uy;   51uy;   62uy;   48uy;   24uy;   14uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character3A -> [0x00uy;   24uy;   24uy; 0x00uy; 0x00uy;   24uy;   24uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character3B -> [0x00uy;   24uy;   24uy; 0x00uy; 0x00uy;   24uy;   24uy;   12uy] |> DirtyGrid.ofByteList Some
        | Character3C -> [  24uy;   12uy;    6uy;    3uy;    6uy;   12uy;   24uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character3D -> [0x00uy; 0x00uy;  126uy; 0x00uy;  126uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character3E -> [   6uy;   12uy;   24uy;   48uy;   24uy;   12uy;    6uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character3F -> [  60uy;  102uy;   48uy;   24uy;   24uy; 0x00uy;   24uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character40 -> [  62uy;   99uy;  123uy;  123uy;   59uy;    3uy;   62uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character41 -> [  12uy;   30uy;   51uy;   51uy;   63uy;   51uy;   51uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character42 -> [  63uy;  102uy;  102uy;   62uy;  102uy;  102uy;   63uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character43 -> [  60uy;  102uy;    3uy;    3uy;    3uy;  102uy;   60uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character44 -> [  31uy;   54uy;  102uy;  102uy;  102uy;   54uy;   31uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character45 -> [ 127uy;   70uy;   22uy;   30uy;   22uy;   70uy;  127uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character46 -> [ 127uy;   70uy;   22uy;   30uy;   22uy;    6uy;   15uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character47 -> [  60uy;  102uy;    3uy;    3uy;  115uy;  102uy;   92uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character48 -> [  51uy;   51uy;   51uy;   63uy;   51uy;   51uy;   51uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character49 -> [  30uy;   12uy;   12uy;   12uy;   12uy;   12uy;   30uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character4A -> [ 120uy;   48uy;   48uy;   48uy;   51uy;   51uy;   30uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character4B -> [ 103uy;  102uy;   54uy;   30uy;   54uy;  102uy;  103uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character4C -> [  15uy;    6uy;    6uy;    6uy;   70uy;  102uy;  127uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character4D -> [  99uy;  119uy;  127uy;  127uy;  107uy;   99uy;   99uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character4E -> [  99uy;  103uy;  111uy;  123uy;  115uy;   99uy;   99uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character4F -> [  28uy;   54uy;   99uy;   99uy;   99uy;   54uy;   28uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character50 -> [  63uy;  102uy;  102uy;   62uy;    6uy;    6uy;   15uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character51 -> [  62uy;   99uy;   99uy;   99uy;  107uy;   62uy;  112uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character52 -> [  63uy;  102uy;  102uy;   62uy;   54uy;  102uy;  103uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character53 -> [  62uy;   99uy;    7uy;   30uy;  112uy;   99uy;   62uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character54 -> [  63uy;   45uy;   12uy;   12uy;   12uy;   12uy;   30uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character55 -> [  51uy;   51uy;   51uy;   51uy;   51uy;   51uy;   63uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character56 -> [  51uy;   51uy;   51uy;   51uy;   51uy;   30uy;   12uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character57 -> [  99uy;   99uy;   99uy;   99uy;  107uy;  127uy;   54uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character58 -> [  99uy;   99uy;   54uy;   28uy;   54uy;   99uy;   99uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character59 -> [  51uy;   51uy;   51uy;   30uy;   12uy;   12uy;   30uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character5A -> [ 127uy;   99uy;   49uy;   24uy;   76uy;  102uy;  127uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character5B -> [  30uy;    6uy;    6uy;    6uy;    6uy;    6uy;   30uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character5C -> [   3uy;    6uy;   12uy;   24uy;   48uy;   96uy;   64uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character5D -> [  30uy;   24uy;   24uy;   24uy;   24uy;   24uy;   30uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character5E -> [   8uy;   28uy;   54uy;   99uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character5F -> [0x00uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy; 0xFFuy] |> DirtyGrid.ofByteList Some
        | Character60 -> [  12uy;   12uy;   24uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character61 -> [0x00uy; 0x00uy;   30uy;   48uy;   62uy;   51uy;  110uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character62 -> [   7uy;    6uy;    6uy;   62uy;  102uy;  102uy;   59uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character63 -> [0x00uy; 0x00uy;   30uy;   51uy;    3uy;   51uy;   30uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character64 -> [  56uy;   48uy;   48uy;   62uy;   51uy;   51uy;  110uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character65 -> [0x00uy; 0x00uy;   30uy;   51uy;   63uy;    3uy;   30uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character66 -> [  28uy;   54uy;   38uy;   15uy;    6uy;    6uy;   15uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character67 -> [0x00uy; 0x00uy;  110uy;   51uy;   51uy;   62uy;   48uy;   31uy] |> DirtyGrid.ofByteList Some
        | Character68 -> [   7uy;    6uy;   54uy;  110uy;  102uy;  102uy;  103uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character69 -> [  12uy; 0x00uy;   14uy;   12uy;   12uy;   12uy;   30uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character6A -> [  48uy; 0x00uy;   56uy;   48uy;   48uy;   51uy;   51uy;   30uy] |> DirtyGrid.ofByteList Some
        | Character6B -> [   7uy;    6uy;  102uy;   54uy;   30uy;   54uy;  103uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character6C -> [  14uy;   12uy;   12uy;   12uy;   12uy;   12uy;   30uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character6D -> [0x00uy; 0x00uy;   51uy;  127uy;  127uy;  107uy;  107uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character6E -> [0x00uy; 0x00uy;   29uy;   51uy;   51uy;   51uy;   51uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character6F -> [0x00uy; 0x00uy;   30uy;   51uy;   51uy;   51uy;   30uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character70 -> [0x00uy; 0x00uy;   59uy;  102uy;  102uy;   62uy;    6uy;   15uy] |> DirtyGrid.ofByteList Some
        | Character71 -> [0x00uy; 0x00uy;  110uy;   51uy;   51uy;   62uy;   48uy;  120uy] |> DirtyGrid.ofByteList Some
        | Character72 -> [0x00uy; 0x00uy;   59uy;  110uy;   70uy;    6uy;   15uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character73 -> [0x00uy; 0x00uy;   62uy;    3uy;   14uy;   56uy;   31uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character74 -> [   8uy;   12uy;   63uy;   12uy;   12uy;   44uy;   24uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character75 -> [0x00uy; 0x00uy;   51uy;   51uy;   51uy;   51uy;  110uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character76 -> [0x00uy; 0x00uy;   51uy;   51uy;   51uy;   30uy;   12uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character77 -> [0x00uy; 0x00uy;   99uy;   99uy;  107uy;  127uy;   54uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character78 -> [0x00uy; 0x00uy;   99uy;   54uy;   28uy;   54uy;   99uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character79 -> [0x00uy; 0x00uy;   51uy;   51uy;   51uy;   62uy;   48uy;   31uy] |> DirtyGrid.ofByteList Some
        | Character7A -> [0x00uy; 0x00uy;   63uy;   25uy;   12uy;   38uy;   63uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character7B -> [  56uy;   12uy;   12uy;    7uy;   12uy;   12uy;   56uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character7C -> [  24uy;   24uy;   24uy; 0x00uy;   24uy;   24uy;   24uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character7D -> [   7uy;   12uy;   12uy;   56uy;   12uy;   12uy;    7uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character7E -> [ 110uy;   59uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character7F -> [0x00uy;    8uy;   28uy;   54uy;   99uy;   99uy;  127uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character80 -> [  62uy;   99uy;    3uy;   99uy;   62uy;   48uy;   96uy;   62uy] |> DirtyGrid.ofByteList Some
        | Character81 -> [0x00uy;   51uy; 0x00uy;   51uy;   51uy;   51uy;  110uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character82 -> [  56uy; 0x00uy;   30uy;   51uy;   63uy;    3uy;   30uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character83 -> [ 126uy;  129uy;   60uy;   96uy;  124uy;  102uy;  220uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character84 -> [  51uy; 0x00uy;   30uy;   48uy;   62uy;   51uy;  110uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character85 -> [   7uy; 0x00uy;   30uy;   48uy;   62uy;   51uy;  110uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character86 -> [  12uy;   12uy;   30uy;   48uy;   62uy;   51uy;  110uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character87 -> [0x00uy; 0x00uy;   62uy;   99uy;    3uy;   30uy;   48uy;   28uy] |> DirtyGrid.ofByteList Some
        | Character88 -> [ 126uy;  129uy;   60uy;  102uy;  126uy;    6uy;   60uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character89 -> [  51uy; 0x00uy;   30uy;   51uy;   63uy;    3uy;   30uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character8A -> [   7uy; 0x00uy;   30uy;   51uy;   63uy;    3uy;   30uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character8B -> [  51uy; 0x00uy;   14uy;   12uy;   12uy;   12uy;   30uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character8C -> [  62uy;   65uy;   28uy;   24uy;   24uy;   24uy;   60uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character8D -> [   7uy; 0x00uy;   14uy;   12uy;   12uy;   12uy;   30uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character8E -> [  99uy;    8uy;   62uy;   99uy;  127uy;   99uy;   99uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character8F -> [  12uy;   12uy; 0x00uy;   30uy;   51uy;   63uy;   51uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character90 -> [  56uy; 0x00uy;   63uy;    6uy;   30uy;    6uy;   63uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character91 -> [0x00uy; 0x00uy;  254uy;   48uy;  254uy;   51uy;  254uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character92 -> [ 124uy;   54uy;   51uy;  127uy;   51uy;   51uy;  115uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character93 -> [  30uy;   33uy; 0x00uy;   30uy;   51uy;   51uy;   30uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character94 -> [0x00uy;   51uy; 0x00uy;   30uy;   51uy;   51uy;   30uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character95 -> [0x00uy;    7uy; 0x00uy;   30uy;   51uy;   51uy;   30uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character96 -> [  30uy;   33uy; 0x00uy;   51uy;   51uy;   51uy;  110uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character97 -> [0x00uy;    7uy; 0x00uy;   51uy;   51uy;   51uy;  110uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character98 -> [0x00uy;   51uy; 0x00uy;   51uy;   51uy;   62uy;   48uy;   31uy] |> DirtyGrid.ofByteList Some
        | Character99 -> [ 195uy;   24uy;   60uy;  102uy;  102uy;   60uy;   24uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character9A -> [  51uy; 0x00uy;   51uy;   51uy;   51uy;   51uy;   30uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character9B -> [  24uy;   24uy;  126uy;    3uy;    3uy;  126uy;   24uy;   24uy] |> DirtyGrid.ofByteList Some
        | Character9C -> [  28uy;   54uy;   38uy;   15uy;    6uy;  103uy;   63uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Character9D -> [  51uy;   51uy;   30uy;   12uy;   63uy;   12uy;   63uy;   12uy] |> DirtyGrid.ofByteList Some
        | Character9E -> [  31uy;   51uy;   51uy;   95uy;   99uy;  243uy;   99uy;  195uy] |> DirtyGrid.ofByteList Some
        | Character9F -> [ 112uy;  216uy;   24uy;   60uy;   24uy;   24uy;   27uy;   14uy] |> DirtyGrid.ofByteList Some
        | CharacterA0 -> [  56uy; 0x00uy;   30uy;   48uy;   62uy;   51uy;  110uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterA1 -> [  28uy; 0x00uy;   14uy;   12uy;   12uy;   12uy;   30uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterA2 -> [0x00uy;   56uy; 0x00uy;   30uy;   51uy;   51uy;   30uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterA3 -> [0x00uy;   56uy; 0x00uy;   51uy;   51uy;   51uy;  110uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterA4 -> [0x00uy;   31uy; 0x00uy;   29uy;   51uy;   51uy;   51uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterA5 -> [  63uy; 0x00uy;   51uy;   55uy;   63uy;   59uy;   51uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterA6 -> [  60uy;   54uy;   54uy;  124uy; 0x00uy;  126uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterA7 -> [  28uy;   54uy;   54uy;   28uy; 0x00uy;   62uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterA8 -> [  24uy; 0x00uy;   24uy;   24uy;   12uy;  102uy;   60uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterA9 -> [0x00uy; 0x00uy; 0x00uy;   63uy;    3uy;    3uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterAA -> [0x00uy; 0x00uy; 0x00uy;   63uy;   48uy;   48uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterAB -> [  99uy;   51uy;   27uy;  108uy;  214uy;   67uy;   33uy;  240uy] |> DirtyGrid.ofByteList Some
        | CharacterAC -> [ 195uy;   99uy;   51uy;  219uy;  236uy;  182uy;  243uy;  192uy] |> DirtyGrid.ofByteList Some
        | CharacterAD -> [  24uy; 0x00uy;   24uy;   24uy;   60uy;   60uy;   24uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterAE -> [0x00uy;  204uy;  102uy;   51uy;  102uy;  204uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterAF -> [0x00uy;   51uy;  102uy;  204uy;  102uy;   51uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterB0 -> [  68uy;   17uy;   68uy;   17uy;   68uy;   17uy;   68uy;   17uy] |> DirtyGrid.ofByteList Some
        | CharacterB1 -> [ 170uy;   85uy;  170uy;   85uy;  170uy;   85uy;  170uy;   85uy] |> DirtyGrid.ofByteList Some
        | CharacterB2 -> [ 187uy;  238uy;  187uy;  238uy;  187uy;  238uy;  187uy;  238uy] |> DirtyGrid.ofByteList Some
        | CharacterB3 -> [  24uy;   24uy;   24uy;   24uy;   24uy;   24uy;   24uy;   24uy] |> DirtyGrid.ofByteList Some
        | CharacterB4 -> [  24uy;   24uy;   24uy;   24uy;   31uy;   24uy;   24uy;   24uy] |> DirtyGrid.ofByteList Some
        | CharacterB5 -> [  24uy;   24uy;   31uy;   24uy;   31uy;   24uy;   24uy;   24uy] |> DirtyGrid.ofByteList Some
        | CharacterB6 -> [ 108uy;  108uy;  108uy;  108uy;  111uy;  108uy;  108uy;  108uy] |> DirtyGrid.ofByteList Some
        | CharacterB7 -> [0x00uy; 0x00uy; 0x00uy; 0x00uy;  127uy;  108uy;  108uy;  108uy] |> DirtyGrid.ofByteList Some
        | CharacterB8 -> [0x00uy; 0x00uy;   31uy;   24uy;   31uy;   24uy;   24uy;   24uy] |> DirtyGrid.ofByteList Some
        | CharacterB9 -> [ 108uy;  108uy;  111uy;   96uy;  111uy;  108uy;  108uy;  108uy] |> DirtyGrid.ofByteList Some
        | CharacterBA -> [ 108uy;  108uy;  108uy;  108uy;  108uy;  108uy;  108uy;  108uy] |> DirtyGrid.ofByteList Some
        | CharacterBB -> [0x00uy; 0x00uy;  127uy;   96uy;  111uy;  108uy;  108uy;  108uy] |> DirtyGrid.ofByteList Some
        | CharacterBC -> [ 108uy;  108uy;  111uy;   96uy;  127uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterBD -> [ 108uy;  108uy;  108uy;  108uy;  127uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterBE -> [  24uy;   24uy;   31uy;   24uy;   31uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterBF -> [0x00uy; 0x00uy; 0x00uy; 0x00uy;   31uy;   24uy;   24uy;   24uy] |> DirtyGrid.ofByteList Some
        | CharacterC0 -> [  24uy;   24uy;   24uy;   24uy;  248uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterC1 -> [  24uy;   24uy;   24uy;   24uy; 0xFFuy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterC2 -> [0x00uy; 0x00uy; 0x00uy; 0x00uy; 0xFFuy;   24uy;   24uy;   24uy] |> DirtyGrid.ofByteList Some
        | CharacterC3 -> [  24uy;   24uy;   24uy;   24uy;  248uy;   24uy;   24uy;   24uy] |> DirtyGrid.ofByteList Some
        | CharacterC4 -> [0x00uy; 0x00uy; 0x00uy; 0x00uy; 0xFFuy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterC5 -> [  24uy;   24uy;   24uy;   24uy; 0xFFuy;   24uy;   24uy;   24uy] |> DirtyGrid.ofByteList Some
        | CharacterC6 -> [  24uy;   24uy;  248uy;   24uy;  248uy;   24uy;   24uy;   24uy] |> DirtyGrid.ofByteList Some
        | CharacterC7 -> [ 108uy;  108uy;  108uy;  108uy;  236uy;  108uy;  108uy;  108uy] |> DirtyGrid.ofByteList Some
        | CharacterC8 -> [ 108uy;  108uy;  236uy;   12uy;  252uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterC9 -> [0x00uy; 0x00uy;  252uy;   12uy;  236uy;  108uy;  108uy;  108uy] |> DirtyGrid.ofByteList Some
        | CharacterCA -> [ 108uy;  108uy;  239uy; 0x00uy; 0xFFuy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterCB -> [0x00uy; 0x00uy; 0xFFuy; 0x00uy;  239uy;  108uy;  108uy;  108uy] |> DirtyGrid.ofByteList Some
        | CharacterCC -> [ 108uy;  108uy;  236uy;   12uy;  236uy;  108uy;  108uy;  108uy] |> DirtyGrid.ofByteList Some
        | CharacterCD -> [0x00uy; 0x00uy; 0xFFuy; 0x00uy; 0xFFuy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterCE -> [ 108uy;  108uy;  239uy; 0x00uy;  239uy;  108uy;  108uy;  108uy] |> DirtyGrid.ofByteList Some
        | CharacterCF -> [  24uy;   24uy; 0xFFuy; 0x00uy; 0xFFuy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterD0 -> [ 108uy;  108uy;  108uy;  108uy; 0xFFuy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterD1 -> [0x00uy; 0x00uy; 0xFFuy; 0x00uy; 0xFFuy;   24uy;   24uy;   24uy] |> DirtyGrid.ofByteList Some
        | CharacterD2 -> [0x00uy; 0x00uy; 0x00uy; 0x00uy; 0xFFuy;  108uy;  108uy;  108uy] |> DirtyGrid.ofByteList Some
        | CharacterD3 -> [ 108uy;  108uy;  108uy;  108uy;  252uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterD4 -> [  24uy;   24uy;  248uy;   24uy;  248uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterD5 -> [0x00uy; 0x00uy;  248uy;   24uy;  248uy;   24uy;   24uy;   24uy] |> DirtyGrid.ofByteList Some
        | CharacterD6 -> [0x00uy; 0x00uy; 0x00uy; 0x00uy;  252uy;  108uy;  108uy;  108uy] |> DirtyGrid.ofByteList Some
        | CharacterD7 -> [ 108uy;  108uy;  108uy;  108uy; 0xFFuy;  108uy;  108uy;  108uy] |> DirtyGrid.ofByteList Some
        | CharacterD8 -> [  24uy;   24uy; 0xFFuy;   24uy; 0xFFuy;   24uy;   24uy;   24uy] |> DirtyGrid.ofByteList Some
        | CharacterD9 -> [  24uy;   24uy;   24uy;   24uy;   31uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterDA -> [0x00uy; 0x00uy; 0x00uy; 0x00uy;  248uy;   24uy;   24uy;   24uy] |> DirtyGrid.ofByteList Some
        | CharacterDB -> [0xFFuy; 0xFFuy; 0xFFuy; 0xFFuy; 0xFFuy; 0xFFuy; 0xFFuy; 0xFFuy] |> DirtyGrid.ofByteList Some
        | CharacterDC -> [0x00uy; 0x00uy; 0x00uy; 0x00uy; 0xFFuy; 0xFFuy; 0xFFuy; 0xFFuy] |> DirtyGrid.ofByteList Some
        | CharacterDD -> [  15uy;   15uy;   15uy;   15uy;   15uy;   15uy;   15uy;   15uy] |> DirtyGrid.ofByteList Some
        | CharacterDE -> [ 240uy;  240uy;  240uy;  240uy;  240uy;  240uy;  240uy;  240uy] |> DirtyGrid.ofByteList Some
        | CharacterDF -> [0xFFuy; 0xFFuy; 0xFFuy; 0xFFuy; 0x00uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterE0 -> [0x00uy; 0x00uy;  110uy;   59uy;   19uy;   59uy;  110uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterE1 -> [  62uy;   99uy;   99uy;   51uy;   99uy;  195uy;  115uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterE2 -> [0x00uy;   63uy;   51uy;    3uy;    3uy;    3uy;    3uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterE3 -> [0x00uy; 0x00uy;  127uy;   54uy;   54uy;   54uy;   54uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterE4 -> [  63uy;   51uy;    6uy;   12uy;    6uy;   51uy;   63uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterE5 -> [0x00uy; 0x00uy;  126uy;   27uy;   27uy;   27uy;   14uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterE6 -> [0x00uy;  102uy;  102uy;  102uy;  102uy;   62uy;    6uy;    3uy] |> DirtyGrid.ofByteList Some
        | CharacterE7 -> [0x00uy;  110uy;   59uy;   24uy;   24uy;   24uy;   24uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterE8 -> [  63uy;   12uy;   30uy;   51uy;   51uy;   30uy;   12uy;   63uy] |> DirtyGrid.ofByteList Some
        | CharacterE9 -> [  28uy;   54uy;   99uy;  127uy;   99uy;   54uy;   28uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterEA -> [  28uy;   54uy;   99uy;   99uy;   54uy;   54uy;  119uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterEB -> [  56uy;   12uy;   24uy;   62uy;   51uy;   51uy;   30uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterEC -> [0x00uy; 0x00uy;  126uy;  219uy;  219uy;  126uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterED -> [  96uy;   48uy;  126uy;  219uy;  219uy;  126uy;    6uy;    3uy] |> DirtyGrid.ofByteList Some
        | CharacterEE -> [  28uy;    6uy;    3uy;   31uy;    3uy;    6uy;   28uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterEF -> [  30uy;   51uy;   51uy;   51uy;   51uy;   51uy;   51uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterF0 -> [0x00uy;  126uy; 0x00uy;  126uy; 0x00uy;  126uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterF1 -> [  24uy;   24uy;  126uy;   24uy;   24uy; 0x00uy;  126uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterF2 -> [   6uy;   12uy;   24uy;   12uy;    6uy; 0x00uy;   63uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterF3 -> [  24uy;   12uy;    6uy;   12uy;   24uy; 0x00uy;   63uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterF4 -> [ 112uy;  216uy;  216uy;   24uy;   24uy;   24uy;   24uy;   24uy] |> DirtyGrid.ofByteList Some
        | CharacterF5 -> [  24uy;   24uy;   24uy;   24uy;   24uy;   27uy;   27uy;   14uy] |> DirtyGrid.ofByteList Some
        | CharacterF6 -> [  24uy;   24uy; 0x00uy;  126uy; 0x00uy;   24uy;   24uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterF7 -> [0x00uy;  110uy;   59uy; 0x00uy;  110uy;   59uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterF8 -> [  28uy;   54uy;   54uy;   28uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterF9 -> [0x00uy; 0x00uy; 0x00uy;   24uy;   24uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterFA -> [0x00uy; 0x00uy; 0x00uy; 0x00uy;   24uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterFB -> [ 240uy;   48uy;   48uy;   48uy;   55uy;   54uy;   60uy;   56uy] |> DirtyGrid.ofByteList Some
        | CharacterFC -> [  26uy;   54uy;   54uy;   54uy;   54uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterFD -> [  14uy;   25uy;   12uy;    6uy;   31uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterFE -> [0x00uy; 0x00uy;   60uy;   60uy;   60uy;   60uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | CharacterFF -> [0x00uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some

module CharacterPattern =
    let ofChar (c:char) : CharacterPattern option =
        match c with
        | '\u0000' -> Character00 |> Some
        | '\u0001' -> Character01 |> Some
        | '\u0002' -> Character02 |> Some
        | '\u0003' -> Character03 |> Some
        | '\u0004' -> Character04 |> Some
        | '\u0005' -> Character05 |> Some
        | '\u0006' -> Character06 |> Some
        | '\u0007' -> Character07 |> Some
        | '\u0008' -> Character08 |> Some
        | '\u0009' -> Character09 |> Some
        | '\u000a' -> Character0A |> Some
        | '\u000b' -> Character0B |> Some
        | '\u000c' -> Character0C |> Some
        | '\u000d' -> Character0D |> Some
        | '\u000e' -> Character0E |> Some
        | '\u000f' -> Character0F |> Some
        | '\u0010' -> Character10 |> Some
        | '\u0011' -> Character11 |> Some
        | '\u0012' -> Character12 |> Some
        | '\u0013' -> Character13 |> Some
        | '\u0014' -> Character14 |> Some
        | '\u0015' -> Character15 |> Some
        | '\u0016' -> Character16 |> Some
        | '\u0017' -> Character17 |> Some
        | '\u0018' -> Character18 |> Some
        | '\u0019' -> Character19 |> Some
        | '\u001a' -> Character1A |> Some
        | '\u001b' -> Character1B |> Some
        | '\u001c' -> Character1C |> Some
        | '\u001d' -> Character1D |> Some
        | '\u001e' -> Character1E |> Some
        | '\u001f' -> Character1F |> Some
        | '\u0020' -> Character20 |> Some
        | '\u0021' -> Character21 |> Some
        | '\u0022' -> Character22 |> Some
        | '\u0023' -> Character23 |> Some
        | '\u0024' -> Character24 |> Some
        | '\u0025' -> Character25 |> Some
        | '\u0026' -> Character26 |> Some
        | '\u0027' -> Character27 |> Some
        | '\u0028' -> Character28 |> Some
        | '\u0029' -> Character29 |> Some
        | '\u002a' -> Character2A |> Some
        | '\u002b' -> Character2B |> Some
        | '\u002c' -> Character2C |> Some
        | '\u002d' -> Character2D |> Some
        | '\u002e' -> Character2E |> Some
        | '\u002f' -> Character2F |> Some
        | '\u0030' -> Character30 |> Some
        | '\u0031' -> Character31 |> Some
        | '\u0032' -> Character32 |> Some
        | '\u0033' -> Character33 |> Some
        | '\u0034' -> Character34 |> Some
        | '\u0035' -> Character35 |> Some
        | '\u0036' -> Character36 |> Some
        | '\u0037' -> Character37 |> Some
        | '\u0038' -> Character38 |> Some
        | '\u0039' -> Character39 |> Some
        | '\u003a' -> Character3A |> Some
        | '\u003b' -> Character3B |> Some
        | '\u003c' -> Character3C |> Some
        | '\u003d' -> Character3D |> Some
        | '\u003e' -> Character3E |> Some
        | '\u003f' -> Character3F |> Some
        | '\u0040' -> Character40 |> Some
        | '\u0041' -> Character41 |> Some
        | '\u0042' -> Character42 |> Some
        | '\u0043' -> Character43 |> Some
        | '\u0044' -> Character44 |> Some
        | '\u0045' -> Character45 |> Some
        | '\u0046' -> Character46 |> Some
        | '\u0047' -> Character47 |> Some
        | '\u0048' -> Character48 |> Some
        | '\u0049' -> Character49 |> Some
        | '\u004a' -> Character4A |> Some
        | '\u004b' -> Character4B |> Some
        | '\u004c' -> Character4C |> Some
        | '\u004d' -> Character4D |> Some
        | '\u004e' -> Character4E |> Some
        | '\u004f' -> Character4F |> Some
        | '\u0050' -> Character50 |> Some
        | '\u0051' -> Character51 |> Some
        | '\u0052' -> Character52 |> Some
        | '\u0053' -> Character53 |> Some
        | '\u0054' -> Character54 |> Some
        | '\u0055' -> Character55 |> Some
        | '\u0056' -> Character56 |> Some
        | '\u0057' -> Character57 |> Some
        | '\u0058' -> Character58 |> Some
        | '\u0059' -> Character59 |> Some
        | '\u005a' -> Character5A |> Some
        | '\u005b' -> Character5B |> Some
        | '\u005c' -> Character5C |> Some
        | '\u005d' -> Character5D |> Some
        | '\u005e' -> Character5E |> Some
        | '\u005f' -> Character5F |> Some
        | '\u0060' -> Character60 |> Some
        | '\u0061' -> Character61 |> Some
        | '\u0062' -> Character62 |> Some
        | '\u0063' -> Character63 |> Some
        | '\u0064' -> Character64 |> Some
        | '\u0065' -> Character65 |> Some
        | '\u0066' -> Character66 |> Some
        | '\u0067' -> Character67 |> Some
        | '\u0068' -> Character68 |> Some
        | '\u0069' -> Character69 |> Some
        | '\u006a' -> Character6A |> Some
        | '\u006b' -> Character6B |> Some
        | '\u006c' -> Character6C |> Some
        | '\u006d' -> Character6D |> Some
        | '\u006e' -> Character6E |> Some
        | '\u006f' -> Character6F |> Some
        | '\u0070' -> Character70 |> Some
        | '\u0071' -> Character71 |> Some
        | '\u0072' -> Character72 |> Some
        | '\u0073' -> Character73 |> Some
        | '\u0074' -> Character74 |> Some
        | '\u0075' -> Character75 |> Some
        | '\u0076' -> Character76 |> Some
        | '\u0077' -> Character77 |> Some
        | '\u0078' -> Character78 |> Some
        | '\u0079' -> Character79 |> Some
        | '\u007a' -> Character7A |> Some
        | '\u007b' -> Character7B |> Some
        | '\u007c' -> Character7C |> Some
        | '\u007d' -> Character7D |> Some
        | '\u007e' -> Character7E |> Some
        | '\u007f' -> Character7F |> Some
        | '\u0080' -> Character80 |> Some
        | '\u0081' -> Character81 |> Some
        | '\u0082' -> Character82 |> Some
        | '\u0083' -> Character83 |> Some
        | '\u0084' -> Character84 |> Some
        | '\u0085' -> Character85 |> Some
        | '\u0086' -> Character86 |> Some
        | '\u0087' -> Character87 |> Some
        | '\u0088' -> Character88 |> Some
        | '\u0089' -> Character89 |> Some
        | '\u008a' -> Character8A |> Some
        | '\u008b' -> Character8B |> Some
        | '\u008c' -> Character8C |> Some
        | '\u008d' -> Character8D |> Some
        | '\u008e' -> Character8E |> Some
        | '\u008f' -> Character8F |> Some
        | '\u0090' -> Character90 |> Some
        | '\u0091' -> Character91 |> Some
        | '\u0092' -> Character92 |> Some
        | '\u0093' -> Character93 |> Some
        | '\u0094' -> Character94 |> Some
        | '\u0095' -> Character95 |> Some
        | '\u0096' -> Character96 |> Some
        | '\u0097' -> Character97 |> Some
        | '\u0098' -> Character98 |> Some
        | '\u0099' -> Character99 |> Some
        | '\u009a' -> Character9A |> Some
        | '\u009b' -> Character9B |> Some
        | '\u009c' -> Character9C |> Some
        | '\u009d' -> Character9D |> Some
        | '\u009e' -> Character9E |> Some
        | '\u009f' -> Character9F |> Some
        | '\u00a0' -> CharacterA0 |> Some
        | '\u00a1' -> CharacterA1 |> Some
        | '\u00a2' -> CharacterA2 |> Some
        | '\u00a3' -> CharacterA3 |> Some
        | '\u00a4' -> CharacterA4 |> Some
        | '\u00a5' -> CharacterA5 |> Some
        | '\u00a6' -> CharacterA6 |> Some
        | '\u00a7' -> CharacterA7 |> Some
        | '\u00a8' -> CharacterA8 |> Some
        | '\u00a9' -> CharacterA9 |> Some
        | '\u00aa' -> CharacterAA |> Some
        | '\u00ab' -> CharacterAB |> Some
        | '\u00ac' -> CharacterAC |> Some
        | '\u00ad' -> CharacterAD |> Some
        | '\u00ae' -> CharacterAE |> Some
        | '\u00af' -> CharacterAF |> Some
        | '\u00b0' -> CharacterB0 |> Some
        | '\u00b1' -> CharacterB1 |> Some
        | '\u00b2' -> CharacterB2 |> Some
        | '\u00b3' -> CharacterB3 |> Some
        | '\u00b4' -> CharacterB4 |> Some
        | '\u00b5' -> CharacterB5 |> Some
        | '\u00b6' -> CharacterB6 |> Some
        | '\u00b7' -> CharacterB7 |> Some
        | '\u00b8' -> CharacterB8 |> Some
        | '\u00b9' -> CharacterB9 |> Some
        | '\u00ba' -> CharacterBA |> Some
        | '\u00bb' -> CharacterBB |> Some
        | '\u00bc' -> CharacterBC |> Some
        | '\u00bd' -> CharacterBD |> Some
        | '\u00be' -> CharacterBE |> Some
        | '\u00bf' -> CharacterBF |> Some
        | '\u00c0' -> CharacterC0 |> Some
        | '\u00c1' -> CharacterC1 |> Some
        | '\u00c2' -> CharacterC2 |> Some
        | '\u00c3' -> CharacterC3 |> Some
        | '\u00c4' -> CharacterC4 |> Some
        | '\u00c5' -> CharacterC5 |> Some
        | '\u00c6' -> CharacterC6 |> Some
        | '\u00c7' -> CharacterC7 |> Some
        | '\u00c8' -> CharacterC8 |> Some
        | '\u00c9' -> CharacterC9 |> Some
        | '\u00ca' -> CharacterCA |> Some
        | '\u00cb' -> CharacterCB |> Some
        | '\u00cc' -> CharacterCC |> Some
        | '\u00cd' -> CharacterCD |> Some
        | '\u00ce' -> CharacterCE |> Some
        | '\u00cf' -> CharacterCF |> Some
        | '\u00d0' -> CharacterD0 |> Some
        | '\u00d1' -> CharacterD1 |> Some
        | '\u00d2' -> CharacterD2 |> Some
        | '\u00d3' -> CharacterD3 |> Some
        | '\u00d4' -> CharacterD4 |> Some
        | '\u00d5' -> CharacterD5 |> Some
        | '\u00d6' -> CharacterD6 |> Some
        | '\u00d7' -> CharacterD7 |> Some
        | '\u00d8' -> CharacterD8 |> Some
        | '\u00d9' -> CharacterD9 |> Some
        | '\u00da' -> CharacterDA |> Some
        | '\u00db' -> CharacterDB |> Some
        | '\u00dc' -> CharacterDC |> Some
        | '\u00dd' -> CharacterDD |> Some
        | '\u00de' -> CharacterDE |> Some
        | '\u00df' -> CharacterDF |> Some
        | '\u00e0' -> CharacterE0 |> Some
        | '\u00e1' -> CharacterE1 |> Some
        | '\u00e2' -> CharacterE2 |> Some
        | '\u00e3' -> CharacterE3 |> Some
        | '\u00e4' -> CharacterE4 |> Some
        | '\u00e5' -> CharacterE5 |> Some
        | '\u00e6' -> CharacterE6 |> Some
        | '\u00e7' -> CharacterE7 |> Some
        | '\u00e8' -> CharacterE8 |> Some
        | '\u00e9' -> CharacterE9 |> Some
        | '\u00ea' -> CharacterEA |> Some
        | '\u00eb' -> CharacterEB |> Some
        | '\u00ec' -> CharacterEC |> Some
        | '\u00ed' -> CharacterED |> Some
        | '\u00ee' -> CharacterEE |> Some
        | '\u00ef' -> CharacterEF |> Some
        | '\u00f0' -> CharacterF0 |> Some
        | '\u00f1' -> CharacterF1 |> Some
        | '\u00f2' -> CharacterF2 |> Some
        | '\u00f3' -> CharacterF3 |> Some
        | '\u00f4' -> CharacterF4 |> Some
        | '\u00f5' -> CharacterF5 |> Some
        | '\u00f6' -> CharacterF6 |> Some
        | '\u00f7' -> CharacterF7 |> Some
        | '\u00f8' -> CharacterF8 |> Some
        | '\u00f9' -> CharacterF9 |> Some
        | '\u00fa' -> CharacterFA |> Some
        | '\u00fb' -> CharacterFB |> Some
        | '\u00fc' -> CharacterFC |> Some
        | '\u00fd' -> CharacterFD |> Some
        | '\u00fe' -> CharacterFE |> Some
        | '\u00ff' -> CharacterFF |> Some
        | _        -> None
