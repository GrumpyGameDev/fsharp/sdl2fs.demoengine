namespace Engine

module Location =
    let translate (x1: int, y1: int) (x2: int, y2: int): int * int = x1 + x2, y1 + y2

    let scale (x1: int, y1: int) (x2: int, y2: int): int * int = x1 * x2, y1 * y2
