namespace Engine

open Utility

type Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter 
        when 'TRoom   : comparison 
        and  'TCounter: comparison
        and  'TToggle : comparison> =
    {
        counters     : Map<'TCounter,int>
        toggles      : Set<'TToggle>
        messages     : MessageLine list
        messageCount : int
        dialogState  : 'TDialogState option
        location     : 'TRoom * int * int
        roomGrids    : Map<'TRoom,(MessageLine list) * DirtyGrid<RoomCell<'TBaseTerrain, 'TCreature, 'TRoom, 'TItem, 'TToggle, 'TCounter>> >
    }
    member x.GetCounter (counter:'TCounter) : int =
        x.counters
        |> Map.tryFind counter
        |> Option.defaultValue 0

    member x.TestCounterCondition (counter:'TCounter) (condition:CounterCondition) : bool =
        (x.GetCounter counter, condition)
        ||> CounterCondition.test

module Engine =
    let private setLocation 
            (x: int, y: int) 
            (engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) 
            : Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter> =
        let room, _, _ = engine.location
        {engine with location = room, x, y}

    let addMessage 
            (message:MessageLine) 
            (engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) 
            : Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter> =
        {engine with 
            messages = 
                [message]
                |> List.append engine.messages
                |> List.rev
                |> List.take (engine.messageCount)
                |> List.rev}

    let getRoom 
            (engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) 
            : 'TRoom =
        let room, _, _ = engine.location
        room

    let private setRoom 
            (room: 'TRoom) 
            (engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) 
            : Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter> =
        let _, x, y = engine.location
        if room<>(engine |> getRoom) then
            engine.roomGrids
            |> Map.tryFind room
            |> Option.fold
                (fun e (messages,_) ->
                    e
                    |> List.foldBack
                        (fun m e' -> 
                            e'
                            |> addMessage m) messages) {engine with location = (room, x, y)}
        else
            engine

    let private tryGetRoomCell (room:'TRoom, x:int, y:int) (engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) : RoomCell<'TBaseTerrain, 'TCreature, 'TRoom, 'TItem, 'TToggle, 'TCounter> option =
        engine.roomGrids
        |> Map.tryFind room
        |> Option.bind
            (snd >> DirtyGrid.tryGet (x,y))

    let private updateRoomGrid (updater:DirtyGrid<RoomCell<'TBaseTerrain, 'TCreature, 'TRoom, 'TItem, 'TToggle, 'TCounter>>->DirtyGrid<RoomCell<'TBaseTerrain, 'TCreature, 'TRoom, 'TItem, 'TToggle, 'TCounter>>) (room:'TRoom) (engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) : Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter> =
        match engine.roomGrids |> Map.tryFind room with
        | Some (messages, roomGrid) ->
            {engine with roomGrids = engine.roomGrids |> Map.add room (messages, roomGrid |> updater)}
        | _ ->
            engine

    let private placeCreature (room:'TRoom, x:int, y:int, creature: 'TCreature option) (engine: Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) : Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter> =
        engine
        |> updateRoomGrid 
            (DirtyGrid.updateCellCommit
                (Option.map (RoomCell.setCreature (creature))) (x,y)) room

    let private removeCreature (room:'TRoom, x:int, y: int) (engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) : ('TCreature option) * Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter> =
        match engine.roomGrids |> Map.tryFind room with
        | Some (_, roomGrid) ->
            match roomGrid.committed |> Map.tryFind (x,y) with
            | Some cell ->
                cell.creature, (engine |> placeCreature (room, x, y , None))
            | None ->
                None, engine
        | None ->
            None, engine

    let internal tryGetAvatarRoomCell (engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) : RoomCell<'TBaseTerrain, 'TCreature, 'TRoom, 'TItem, 'TToggle, 'TCounter> option =
        tryGetRoomCell (engine.location) engine

    let internal placeAvatarCreature (creatureifier:'TAvatar->'TCreature option) (descriptor:'TAvatar) (engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) : Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter> =
        let room, x, y = engine.location
        engine
        |> placeCreature (room, x, y, descriptor |> creatureifier) 

    let internal removeAvatarCreature (avatarifier:'TCreature option -> 'TAvatar option) (engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) : ('TAvatar option) * Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter> =
        let c, e = 
            engine 
            |> removeCreature engine.location
        c |> avatarifier, e

    let private getRoomCell (room:'TRoom, x:int, y:int) (engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) : RoomCell<'TBaseTerrain, 'TCreature, 'TRoom, 'TItem, 'TToggle, 'TCounter> option =
        engine.roomGrids 
        |> Map.tryFind room
        |> Option.map snd
        |> Option.bind
            (fun roomGrid -> roomGrid.committed |> Map.tryFind (x,y))

    let getAvatarCreature (avatarifier:'TCreature option -> 'TAvatar option) (engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) : 'TAvatar option =
        engine
        |> getRoomCell (engine.location)
        |> Option.bind
            (fun roomCell ->
                roomCell.creature |> avatarifier)
    
    let create
            (creatureifier: 'TAvatar->'TCreature option) 
            (locationSource:unit->'TRoom * int * int)
            (roomsSource:unit->Map<'TRoom,(MessageLine list) * DirtyGrid<RoomCell<'TBaseTerrain, 'TCreature, 'TRoom, 'TItem, 'TToggle, 'TCounter>>>) 
            (avatarSource:unit->'TAvatar)
            (welcome: MessageLine list)
            (messageCount:int) =
        {
            dialogState  = None
            location     = locationSource()
            roomGrids    = roomsSource()
            messageCount = messageCount
            messages     = [for i=1 to messageCount do yield MessageLine.empty]
            toggles      = Set.empty
            counters     = Map.empty
        }
        |> placeAvatarCreature creatureifier (avatarSource())
        |> List.foldBack 
            (fun m e -> e |> addMessage m) (welcome |> List.rev)

    let private updateLocation (updater:(int*int)->(int*int)) (engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) : Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter> =
        let room, x, y = engine.location
        let nextX, nextY = (x,y) |> updater
        {engine with location = room, nextX, nextY}

    let private setToggle 
            (toggle:'TToggle) 
            (engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) 
            : Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter> =
        {engine with toggles = engine.toggles |> Set.add toggle}

    let private clearToggle 
            (toggle:'TToggle) 
            (engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) 
            : Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter> =
        {engine with toggles = engine.toggles |> Set.remove toggle}

    let private flipToggle 
            (toggle:'TToggle) 
            (engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) 
            : Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter> =
        if engine.toggles.Contains toggle then
            engine 
            |> clearToggle toggle
        else
            engine 
            |> setToggle toggle

    let private setCounter
            (counterClamper: 'TCounter->int->int) 
            (counter:'TCounter) 
            (value: int)
            (engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) 
            : Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter> =
        {engine with counters = engine.counters |> Map.add counter (value |> counterClamper counter)}
    
    let private changeCounter 
            (counterClamper: 'TCounter->int->int) 
            (counter:'TCounter) 
            (value: int)
            (engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) 
            : Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter> =
        let newValue =
            engine.GetCounter counter
            |> (+) value
        engine
        |> setCounter counterClamper counter newValue

    let private applyTriggerEffect 
            (counterClamper: 'TCounter->int->int) 
            (effect:TriggerEffect<'TRoom, 'TToggle, 'TCounter>) 
            (descriptor:'TAvatar option, engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) 
            : ('TAvatar option)* Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter> =
        match descriptor.IsSome, effect with
        | false, _ -> 
            (descriptor, engine)
        | true, SetCounter (counter, value) ->
            (descriptor,
                engine
                |> setCounter counterClamper counter value)
        | true, ChangeCounter (counter, value) ->
            (descriptor,
                engine
                |> changeCounter counterClamper counter value)
        | true, SetToggle (toggle) ->
            (descriptor,
                engine
                |> setToggle toggle)
        | true, ClearToggle (toggle) ->
            (descriptor,
                engine
                |> clearToggle toggle)
        | true, FlipToggle (toggle) ->
            (descriptor,
                engine
                |> flipToggle toggle)
        | true, AddMessages messages ->
            (descriptor,
                engine
                |> List.foldBack
                    (fun i a -> 
                        a
                        |> addMessage i) (messages |> List.rev))
        | true, MoveTo (room, location) ->
            (descriptor,
                engine
                |> setRoom room
                |> setLocation location)

    let rec private applyTriggerEffects 
            (counterClamper: 'TCounter->int->int) 
            (creatureifier:'TAvatar->'TCreature option) 
            (effects:TriggerEffect<'TRoom, 'TToggle, 'TCounter> list) 
            (descriptor:'TAvatar option, 
                engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) 
            : Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter> =
        match descriptor.IsSome, effects with
        | false, _ ->
            engine
        | true, [] ->
            engine
            |> placeAvatarCreature creatureifier (descriptor |> Option.get)
        | true, effect :: tail ->
            (descriptor, engine)
            |> applyTriggerEffect counterClamper effect
            |> applyTriggerEffects counterClamper creatureifier tail

    let private standardMoveAvatar 
            (counterConditionResolver: 'TCounter -> CounterCondition -> bool)
            (toggleResolver:'TToggle->bool) 
            (creatureifier:'TAvatar->'TCreature option) 
            (direction:Direction) 
            (descriptor:'TAvatar
                ,engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) 
            : Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter> =
        let room, x, y = engine.location
        let nextX, nextY = 
            direction 
            |> Direction.step (x,y)
        engine 
        |> getRoomCell (room, nextX, nextY)
        |> Option.fold
            (fun e' c -> 
                if c.terrain |> Terrain.isObstacle counterConditionResolver toggleResolver then
                    e'
                    |> placeAvatarCreature creatureifier descriptor
                else
                    e'
                    |> setLocation (nextX, nextY)
                    |> placeAvatarCreature creatureifier descriptor
                ) engine

    let private onAvatarExit 
            (counterConditionResolver: 'TCounter -> CounterCondition -> bool)
            (toggleResolver:'TToggle->bool) 
            (counterClamper: 'TCounter->int->int) 
            (creatureifier:'TAvatar->'TCreature option) 
            (avatarifier:'TCreature option->'TAvatar option) 
            (getFacing:'TAvatar->Direction)
            (setFacing:Direction->'TAvatar->'TAvatar) 
            (direction:Direction) 
            (engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) 
            : Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter> =
        let exitTriggerEffects =
            engine
            |> tryGetAvatarRoomCell
            |> Option.map (RoomCell.getTerrain)
            |> Option.map (Terrain.getOnExitEffects engine.TestCounterCondition engine.toggles.Contains direction)
            |> Option.defaultValue []
        match engine |> removeAvatarCreature avatarifier with
        | Some d, e ->
            if (d |> getFacing) = direction then
                if exitTriggerEffects.IsEmpty then
                    (d,e)
                    |> standardMoveAvatar counterConditionResolver toggleResolver creatureifier direction
                else
                    (d |> Some, e) 
                    |> applyTriggerEffects counterClamper creatureifier exitTriggerEffects
            else
                engine
                |> placeAvatarCreature creatureifier (d |> setFacing direction)
        | _ ->
            engine

    let rec private onAvatarEnter 
            (counterClamper: 'TCounter->int->int) 
            (creatureifier:'TAvatar->'TCreature option) 
            (avatarifier:'TCreature option->'TAvatar option) 
            (originalLocation:'TRoom * int * int) 
            (engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) 
            : Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter> =
        if engine.location <> originalLocation then
            let enterTriggerEffects =
                engine
                |> tryGetAvatarRoomCell
                |> Option.map (RoomCell.getTerrain)
                |> Option.map (Terrain.getOnEnterEffects engine.TestCounterCondition engine.toggles.Contains)
                |> Option.defaultValue []
            engine
            |> removeAvatarCreature avatarifier
            |> applyTriggerEffects counterClamper creatureifier enterTriggerEffects
            |> onAvatarEnter counterClamper creatureifier avatarifier engine.location
        else
            engine

    let private moveAvatar 
            (counterConditionResolver: 'TCounter -> CounterCondition -> bool)
            (toggleResolver:'TToggle->bool) 
            (counterClamper: 'TCounter->int->int) 
            (creatureifier:'TAvatar->'TCreature option) 
            (avatarifier:'TCreature option->'TAvatar option) 
            (getFacing:'TAvatar->Direction)
            (setFacing:Direction->'TAvatar->'TAvatar) 
            (direction:Direction) 
            (engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) 
            : Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter> =
        let originalLocation = engine.location
        engine
        |> onAvatarExit counterConditionResolver toggleResolver counterClamper creatureifier avatarifier getFacing setFacing direction
        |> onAvatarEnter counterClamper creatureifier avatarifier originalLocation

    let getInteractLocation (avatarifier:'TCreature option->'TAvatar option) (getFacing:'TAvatar->Direction) (engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) : (int*int) option =
        let _, x, y = engine.location
        engine 
        |> getAvatarCreature avatarifier
        |> Option.map
            (getFacing 
                >> Direction.step (x,y))

    let private getInteractRoomCell (avatarifier: 'TCreature option->'TAvatar option) (getFacing:'TAvatar->Direction) (engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) : RoomCell<'TBaseTerrain, 'TCreature, 'TRoom, 'TItem, 'TToggle, 'TCounter> option =
        engine
        |> getInteractLocation avatarifier getFacing
        |> Option.bind
            (fun (x,y) ->
                engine
                |> getRoomCell (engine |> getRoom, x, y))

    let rec private interactAvatarTerrain 
            (counterConditionResolver: 'TCounter -> CounterCondition -> bool)
            (toggleResolver:'TToggle->bool) 
            (counterClamper: 'TCounter->int->int) 
            (creatureifier:'TAvatar->'TCreature option) 
            (terrain:Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>) 
            (descriptor: 'TAvatar option, engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) 
            : Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter> =
        let effects =
            terrain
            |> Terrain.getOnInteractEffects counterConditionResolver toggleResolver

        (descriptor, engine)
        |> applyTriggerEffects counterClamper creatureifier effects

    let private interactAvatar 
            (counterConditionResolver: 'TCounter -> CounterCondition -> bool)
            (toggleResolver:'TToggle->bool) 
            (counterClamper: 'TCounter->int->int) 
            (creatureifier:'TAvatar->'TCreature option) 
            (avatarifier: 'TCreature option->'TAvatar option) 
            (getFacing:'TAvatar->Direction) 
            (engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) 
            : Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter> =
        engine
        |> getInteractRoomCell avatarifier getFacing
        |> Option.fold
            (fun e c -> 
                match c.creature, c.item, c.terrain with
                | None, None, terrain ->
                    e
                    |> removeAvatarCreature avatarifier
                    |> interactAvatarTerrain counterConditionResolver toggleResolver counterClamper creatureifier terrain
                | _ ->
                    e) engine

    let setDialogState 
            (dialogState:'TDialogState option) 
            (engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) 
            : Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter> =
        {engine with dialogState = dialogState}

    let private doInPlayCommand 
            (counterClamper: 'TCounter->int->int) 
            (creatureifier:'TAvatar->'TCreature option) 
            (avatarifier:'TCreature option->'TAvatar option) 
            (getFacing:'TAvatar->Direction)
            (setFacing:Direction->'TAvatar->'TAvatar) 
            (defaultCommandHandler: Command->Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>->Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter> option)
            (command:Command) 
            (engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) 
            : Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter> option=
        match command with
        | Up ->
            engine
            |> moveAvatar engine.TestCounterCondition engine.toggles.Contains counterClamper creatureifier avatarifier getFacing setFacing North
            |> Some
        | Down ->
            engine
            |> moveAvatar engine.TestCounterCondition engine.toggles.Contains counterClamper creatureifier avatarifier getFacing setFacing South
            |> Some
        | Left ->
            engine
            |> moveAvatar engine.TestCounterCondition engine.toggles.Contains counterClamper creatureifier avatarifier getFacing setFacing West
            |> Some
        | Right ->
            engine
            |> moveAvatar engine.TestCounterCondition engine.toggles.Contains counterClamper creatureifier avatarifier getFacing setFacing East
            |> Some
        | Blue ->
            engine
            |> interactAvatar engine.TestCounterCondition engine.toggles.Contains counterClamper creatureifier avatarifier getFacing
            |> Some
        | x ->
            engine
            |> defaultCommandHandler x


    let doCommand 
            (counterClamper: 'TCounter->int->int) 
            (dialogHandler:'TDialogState->Command->Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>->Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter> option)
            (creatureifier:'TAvatar->'TCreature option) 
            (avatarifier:'TCreature option->'TAvatar option) 
            (getFacing:'TAvatar->Direction)
            (setFacing:Direction->'TAvatar->'TAvatar) 
            (defaultCommandHandler: Command->Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>->Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter> option)
            (command:Command) 
            (engine:Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter>) 
            : Engine<'TBaseTerrain, 'TCreature, 'TAvatar, 'TRoom, 'TItem, 'TToggle, 'TDialogState, 'TCounter> option=
        match engine.dialogState with
        | None ->
            engine
            |> doInPlayCommand counterClamper creatureifier avatarifier getFacing setFacing defaultCommandHandler command
        | Some dialog ->
            engine
            |> dialogHandler dialog command
