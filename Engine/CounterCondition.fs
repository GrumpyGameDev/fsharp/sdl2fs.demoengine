namespace Engine

type CounterCondition =
    | Equals of int
    | GreaterThan of int
    | LessThan of int
    | Not of CounterCondition
    | And of CounterCondition * CounterCondition
    | Or of CounterCondition * CounterCondition

module internal CounterCondition =
    let rec test (value: int) (condition: CounterCondition): bool =
        match condition with
        | Equals other -> value = other
        | GreaterThan other -> value > other
        | LessThan other -> value < other
        | Not subcondition ->
            (value, subcondition)
            ||> test
            |> not
        | And(subconditionA, subconditionB) -> ((value, subconditionA) ||> test) && ((value, subconditionB) ||> test)
        | Or(subconditionA, subconditionB) -> ((value, subconditionA) ||> test) || ((value, subconditionB) ||> test)
