namespace Engine

type Command =
    | Up
    | Down
    | Left
    | Right
    | Green
    | Red
    | Blue
    | Yellow
    | Back
    | Start
    | Previous
    | Next
