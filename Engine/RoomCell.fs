namespace Engine

type RoomCell<'TBaseTerrain, 'TCreature, 'TRoom, 'TItem, 'TToggle, 'TCounter> =
    { terrain: Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>
      creature: 'TCreature option
      item: 'TItem option }

module RoomCell =
    let create (terrain: Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>, creature: 'TCreature option,
                item: 'TItem option): RoomCell<'TBaseTerrain, 'TCreature, 'TRoom, 'TItem, 'TToggle, 'TCounter> =
        { terrain = terrain
          creature = creature
          item = item }

    let internal setCreature (creature: 'TCreature option)
        (roomCell: RoomCell<'TBaseTerrain, 'TCreature, 'TRoom, 'TItem, 'TToggle, 'TCounter>): RoomCell<'TBaseTerrain, 'TCreature, 'TRoom, 'TItem, 'TToggle, 'TCounter> =
        { roomCell with creature = creature }

    let internal getCreature (roomCell: RoomCell<'TBaseTerrain, 'TCreature, 'TRoom, 'TItem, 'TToggle, 'TCounter>): 'TCreature option =
        roomCell.creature

    let internal setTerrain (terrain: Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>)
        (roomCell: RoomCell<'TBaseTerrain, 'TCreature, 'TRoom, 'TItem, 'TToggle, 'TCounter>): RoomCell<'TBaseTerrain, 'TCreature, 'TRoom, 'TItem, 'TToggle, 'TCounter> =
        { roomCell with terrain = terrain }

    let internal getTerrain (roomCell: RoomCell<'TBaseTerrain, 'TCreature, 'TRoom, 'TItem, 'TToggle, 'TCounter>): Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter> =
        roomCell.terrain

    let updateTerrain (updater: Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter> -> Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>)
        (roomCell: RoomCell<'TBaseTerrain, 'TCreature, 'TRoom, 'TItem, 'TToggle, 'TCounter>): RoomCell<'TBaseTerrain, 'TCreature, 'TRoom, 'TItem, 'TToggle, 'TCounter> =
        roomCell
        |> setTerrain
            (roomCell
             |> getTerrain
             |> updater)

    let internal setItem (item: 'TItem option)
        (roomCell: RoomCell<'TBaseTerrain, 'TCreature, 'TRoom, 'TItem, 'TToggle, 'TCounter>): RoomCell<'TBaseTerrain, 'TCreature, 'TRoom, 'TItem, 'TToggle, 'TCounter> =
        { roomCell with item = item }

    let internal getItem (roomCell: RoomCell<'TBaseTerrain, 'TCreature, 'TRoom, 'TItem, 'TToggle, 'TCounter>): 'TItem option =
        roomCell.item
