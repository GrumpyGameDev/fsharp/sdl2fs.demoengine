namespace Engine

type Direction =
    | North
    | East
    | South
    | West

module Direction =
    let step (x: int, y: int) (direction: Direction): int * int =
        match direction with
        | North -> (x, y - 1)
        | East -> (x + 1, y)
        | South -> (x, y + 1)
        | West -> (x - 1, y)
