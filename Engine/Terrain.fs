namespace Engine

type Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter> =
    | Base of 'TBaseTerrain
    
    | Obstacle of Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>
    | Toggled of ('TToggle) * (Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>) * (Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>)
    | CounterCondition of ('TCounter) * (CounterCondition) * (Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>) * (Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>)
    | Trigger of (Trigger<'TRoom, 'TToggle, 'TCounter>) * (Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>)


module Terrain =
    let toCounterCondition
            (counter:'TCounter)
            (condition: CounterCondition)
            (whenFalse: Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>, 
                whenTrue: Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>)
            : Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter> =
        (counter, condition, whenFalse, whenTrue) |> CounterCondition

    let tryGetCounterCondition 
            (terrain:Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>)
            : (('TCounter) * (CounterCondition) * (Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>) * (Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>)) option =
        match terrain with
        | CounterCondition (a,b,c,d) ->
            (a,b,c,d) |> Some
        | _ ->
            None    

    let toToggled 
            (toggle:'TToggle) 
            (untoggled:Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>, 
                toggled:Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>)
            : Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter> = 
        (toggle, untoggled, toggled) |> Toggled

    let tryGetToggled 
            (terrain:Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>)
            : (('TToggle) * (Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>) * (Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>)) option =
        match terrain with
        | Toggled (a,b,c) ->
            (a,b,c) |> Some
        | _ ->
            None    

    let toObstacle 
            (terrain:Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>) 
            : Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>=
        terrain |> Obstacle

    let rec isObstacle
            (counterConditionResolver: 'TCounter -> CounterCondition -> bool)
            (toggleResolver:'TToggle->bool) 
            (terrain:Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>)
            : bool =
        match terrain with
        | Base _ ->
            false
        | Obstacle _ ->
            true
        | Toggled (toggle, untoggled, toggled) ->
            if toggle |> toggleResolver then
                toggled
                |> isObstacle counterConditionResolver toggleResolver
            else
                untoggled
                |> isObstacle counterConditionResolver toggleResolver
        | CounterCondition (counter,condition,whenFalse,whenTrue) ->
            if counterConditionResolver counter condition then
                whenTrue
                |> isObstacle counterConditionResolver toggleResolver
            else
                whenFalse
                |> isObstacle counterConditionResolver toggleResolver
        | Trigger (_,t) ->
            t |> isObstacle counterConditionResolver toggleResolver


    let toTrigger 
            (trigger:Trigger<'TRoom, 'TToggle, 'TCounter>) 
            (terrain:Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>) 
            : Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter> =
        (trigger, terrain) 
        |> Trigger

    let tryGetTriggler
            (terrain:Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>) 
            : (Trigger<'TRoom, 'TToggle, 'TCounter> * Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>) option =
        match terrain with
        | Trigger (a,b) ->
            (a,b) |> Some
        | _ ->
            None


    let rec private buildTriggerEffects
            (effectsExtractor:Trigger<'TRoom, 'TToggle, 'TCounter>->TriggerEffect<'TRoom, 'TToggle, 'TCounter> option) 
            (counterConditionResolver: 'TCounter -> CounterCondition -> bool)
            (toggleResolver:'TToggle->bool) 
            (terrain:Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>) 
            (effects:TriggerEffect<'TRoom, 'TToggle, 'TCounter> list) 
            : TriggerEffect<'TRoom, 'TToggle, 'TCounter> list =
        match terrain with
        | Obstacle t -> 
            effects
            |> buildTriggerEffects effectsExtractor counterConditionResolver toggleResolver t
        | CounterCondition (counter, condition, whenFalse, whenTrue) ->
            if counterConditionResolver counter condition then
                effects
                |> buildTriggerEffects effectsExtractor counterConditionResolver toggleResolver whenTrue
            else
                effects
                |> buildTriggerEffects effectsExtractor counterConditionResolver toggleResolver whenFalse
        | Toggled (toggle, untoggled, toggled) ->
            if toggle |> toggleResolver then
                effects
                |> buildTriggerEffects effectsExtractor counterConditionResolver toggleResolver toggled
            else
                effects
                |> buildTriggerEffects effectsExtractor counterConditionResolver toggleResolver untoggled
        | Trigger (trigger, t)->
            trigger
            |> effectsExtractor
            |> Option.fold
                (fun a i -> 
                    [i]
                    |> List.append a) effects
            |> buildTriggerEffects effectsExtractor counterConditionResolver toggleResolver t
        | _ ->
            effects

    let getOnExitEffects 
            (counterConditionResolver: 'TCounter -> CounterCondition -> bool)
            (toggleResolver:'TToggle->bool) 
            (direction:Direction) 
            (terrain:Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>) 
            : TriggerEffect<'TRoom, 'TToggle, 'TCounter> list =
        []
        |> buildTriggerEffects (Trigger.tryGetOnExitEffect direction) counterConditionResolver toggleResolver terrain

    let getOnEnterEffects 
            (counterConditionResolver: 'TCounter -> CounterCondition -> bool)
            (toggleResolver:'TToggle->bool) 
            (terrain:Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>) 
            : TriggerEffect<'TRoom, 'TToggle, 'TCounter> list =
        []
        |> buildTriggerEffects Trigger.tryGetOnEnterEffect counterConditionResolver toggleResolver terrain

    let getOnInteractEffects 
            (counterConditionResolver: 'TCounter -> CounterCondition -> bool)
            (toggleResolver:'TToggle->bool) 
            (terrain:Terrain<'TBaseTerrain, 'TRoom, 'TToggle, 'TCounter>) 
            : TriggerEffect<'TRoom, 'TToggle, 'TCounter> list =
        []
        |> buildTriggerEffects Trigger.tryGetOnInteractEffect counterConditionResolver toggleResolver terrain
