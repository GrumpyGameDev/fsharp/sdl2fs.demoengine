namespace Engine

type MessageLineType = 
  | Info

type MessageLine =
    { text: string
      messageLineType: MessageLineType }

module MessageLine =
    let create (messageType: MessageLineType, text: string): MessageLine =
        { text = text
          messageLineType = messageType }

    let empty = (Info, "") |> create
