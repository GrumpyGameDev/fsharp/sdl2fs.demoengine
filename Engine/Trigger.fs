namespace Engine

type Trigger<'TRoom, 'TToggle, 'TCounter> =
    | OnExit of Direction * TriggerEffect<'TRoom, 'TToggle, 'TCounter>
    | OnEnter of TriggerEffect<'TRoom, 'TToggle, 'TCounter>
    | OnInteract of TriggerEffect<'TRoom, 'TToggle, 'TCounter>

module Trigger =
    let toOnExit 
            (direction: Direction) 
            (effect: TriggerEffect<'TRoom, 'TToggle, 'TCounter>)
            : Trigger<'TRoom, 'TToggle, 'TCounter> =
        (direction, effect)
        |> OnExit

    let toOnEnter 
            (effect: TriggerEffect<'TRoom, 'TToggle, 'TCounter>)
            : Trigger<'TRoom, 'TToggle, 'TCounter> =
        effect 
        |> OnEnter

    let tryGetOnExitEffect
            (direction:Direction)
            (trigger:Trigger<'TRoom, 'TToggle, 'TCounter>)
            : TriggerEffect<'TRoom, 'TToggle, 'TCounter> option=
        match trigger with
        | OnExit (d,e) when d = direction->
            e |> Some
        | _ ->
            None

    let tryGetOnEnterEffect
            (trigger:Trigger<'TRoom, 'TToggle, 'TCounter>)
            : TriggerEffect<'TRoom, 'TToggle, 'TCounter> option=
        match trigger with
        | OnEnter e->
            e |> Some
        | _ ->
            None

    let tryGetOnInteractEffect
            (trigger:Trigger<'TRoom, 'TToggle, 'TCounter>)
            : TriggerEffect<'TRoom, 'TToggle, 'TCounter> option=
        match trigger with
        | OnInteract e->
            e |> Some
        | _ ->
            None

    let toOnInteract 
            (effect: TriggerEffect<'TRoom, 'TToggle, 'TCounter>)
            : Trigger<'TRoom, 'TToggle, 'TCounter> =
        effect 
        |> OnInteract

