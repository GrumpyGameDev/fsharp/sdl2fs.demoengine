namespace Engine

type TriggerEffect<'TRoom, 'TToggle, 'TCounter> =
    | MoveTo of 'TRoom * (int * int)

    | SetToggle of 'TToggle
    | ClearToggle of 'TToggle
    | FlipToggle of 'TToggle

    | SetCounter of 'TCounter * int
    | ChangeCounter of 'TCounter * int

    | AddMessages of MessageLine list
