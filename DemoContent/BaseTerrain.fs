namespace DemoContent

open Engine

type BaseTerrain =
    | Grass
    | Road
    | Tree
    | Floor
    | Wall
    | Sign
    | Door of bool
