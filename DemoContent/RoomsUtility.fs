namespace DemoContent

module RoomsUtility =
    open Utility
    open Engine

    let internal columns, rows = ContentConstants.roomSize

    let private northExits = [ 0 .. (columns - 1) ] |> List.map (fun x -> (x, 0))
    let private southExits = northExits |> List.map (fun (x, _) -> (x, rows - 1))
    let private westExits = northExits |> List.map (fun (x, y) -> (y, x))
    let private eastExits = westExits |> List.map (fun (_, y) -> (columns - 1, y))

    let private toSouth (x: int, y: int): int * int = x, rows - 1

    let private toNorth (x: int, y: int): int * int = x, 0

    let private toEast (x: int, y: int): int * int = columns - 1, y

    let private toWest (x: int, y: int): int * int = 0, y

    let private stitch (walker: int * int -> (int * int)) (exits: (int * int) list) (direction: Direction)
        (destination: Room) =
        List.foldBack (fun (x, y) grid ->
            grid
            |> DirtyGrid.mapCellCommit
                ((destination, (x, y) |> walker)
                 |> MoveTo
                 |> Trigger.toOnExit direction
                 |> Terrain.toTrigger
                 |> RoomCell.updateTerrain) (x, y)) exits

    let internal stitchNorth (destination: Room) = stitch toSouth northExits North destination

    let internal stitchSouth (destination: Room) = stitch toNorth southExits South destination

    let internal stitchEast (destination: Room) = stitch toWest eastExits East destination

    let internal stitchWest (destination: Room) = stitch toEast westExits West destination
