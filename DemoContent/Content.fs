namespace DemoContent

module Content =
    open Engine

    type DemoEngine = Engine<BaseTerrain, Creature, Avatar, Room, Item, Toggle, DialogState, Counter>

    let creatureifier (avatar: Avatar): Creature option =
        avatar
        |> Tagon
        |> Some


    let defaultCommandHandler (command: Command) (engine: DemoEngine): DemoEngine option =
        match command with
        | Back ->
            engine
            |> Engine.setDialogState
                (Resume
                 |> MainMenu
                 |> Some)
            |> Some
        | _ -> engine |> Some

    let doMainMenuCommand (state: MainMenuState) (command: Command) (engine: DemoEngine): DemoEngine option =
        match command with
        | Back ->
            engine
            |> Engine.setDialogState None
            |> Some
        | Down ->
            engine
            |> Engine.setDialogState
                (state.Next
                 |> MainMenu
                 |> Some)
            |> Some
        | Up ->
            engine
            |> Engine.setDialogState
                (state.Previous
                 |> MainMenu
                 |> Some)
            |> Some
        | _ -> engine |> Some

    let dialogHandler (dialog: DialogState) (command: Command) (engine: DemoEngine): DemoEngine option =
        match dialog with
        | MainMenu state -> engine |> doMainMenuCommand state command

    let counterClamper (_: Counter) (value: int): int = value

    let avatarifier (creature: Creature option): Avatar option =
        creature |> Option.bind (function
                        | Tagon a -> a |> Some)

    let createAvatar(): Avatar = South |> Avatar.create

    let welcome =
        [
            (Info, "Navigate to the '?' sign with") |> MessageLine.create
            (Info, "the arrows keys and press A to") |> MessageLine.create
            (Info, "interact. Game controller d-pad") |> MessageLine.create
            (Info, "and 'X' button also work.") |> MessageLine.create
        ]


    let initialLocation() = (Room.Start, 9, 9)
