namespace DemoContent

module Rooms =
    open Utility
    open Engine

    type DemoRoom = DirtyGrid<RoomCell<BaseTerrain, Creature, Room, Item, Toggle, Counter>>

    let private columns = RoomsUtility.columns
    let private rows = RoomsUtility.rows
    let private roomSize = ContentConstants.roomSize

    let private dungeonTemplate =
        DirtyGrid.create()
        |> DirtyGrid.putRect 
            (0,0) 
            (roomSize) 
            ((Wall |> Base |> Obstacle, None, None) |> RoomCell.create |> Some)
        |> DirtyGrid.putRect 
            (1,1) 
            (columns-2, rows-2) 
            ((Floor |> Base, None, None) |> RoomCell.create |> Some)

    let private setMessages 
            (messages:MessageLine list) 
            (grid: DemoRoom) 
            : (MessageLine list) * DemoRoom =
        messages, grid

    let private start : (MessageLine list) * DemoRoom =
        let tutorialSign = 
            Sign 
            |> Base
            |> Obstacle
            |> Terrain.toTrigger 
                ([
                    (Info, "A door appears.") |> MessageLine.create
                    (Info, "Go over and open it by interact-") |> MessageLine.create
                    (Info, "ing with it.") |> MessageLine.create
                    (Info, "Maybe it's the way out...") |> MessageLine.create
                ] |> AddMessages |> Trigger.toOnInteract)
            |> Terrain.toTrigger
                (ReadTutorialSign |> SetToggle |> Trigger.toOnInteract)

        let door = 
            (Floor |> Base, true |> Door |> Base |> Obstacle)
            |> Terrain.toToggled ReadTutorialSign

        dungeonTemplate
        |> DirtyGrid.put
            (columns/2,rows/2) 
            ((tutorialSign, None, None) 
            |> RoomCell.create 
            |> Some)
        |> DirtyGrid.put
            (columns-2,1) 
            ((door, None, None) 
            |> RoomCell.create 
            |> Some)
        |> DirtyGrid.commit

        |> setMessages []

    let create() : Map<Room, (MessageLine list) * DemoRoom> =
        [
            Room.Start, start
        ]
        |> Map.ofList

