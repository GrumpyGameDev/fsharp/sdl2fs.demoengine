namespace DemoContent

open Engine

type Avatar =
    { facing: Direction }

module Avatar =
    let setFacing (facing: Direction) (descriptor: Avatar): Avatar = { facing = facing }

    let getFacing (descriptor: Avatar): Direction = descriptor.facing

    let internal create (facing: Direction): Avatar = { facing = facing }
