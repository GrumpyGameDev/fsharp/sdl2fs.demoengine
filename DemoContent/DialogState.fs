namespace DemoContent

type MainMenuState =
    | Resume
    | New
    | Load
    | Help
    | About
    | Options
    | Quit

    member x.Next: MainMenuState =
        match x with
        | Resume -> New
        | New -> Load
        | Load -> Help
        | Help -> About
        | About -> Options
        | Options -> Quit
        | Quit -> Resume

    member x.Previous: MainMenuState =
        match x with
        | Resume -> Quit
        | New -> Resume
        | Load -> New
        | Help -> Load
        | About -> Help
        | Options -> About
        | Quit -> Options

type DialogState = MainMenu of MainMenuState
