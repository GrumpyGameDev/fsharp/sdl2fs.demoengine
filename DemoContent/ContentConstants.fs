namespace DemoContent

module ContentConstants =
    let roomColumns = 20
    let roomRows = 20
    let roomSize = (roomColumns, roomRows)
