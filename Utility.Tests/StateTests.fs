module Utility.Tests.State

open Xunit
open Utility

[<Fact>]
let ``State.create creates a state`` () =
    let data = 0
    let context = "zero"

    let state = (data, context) |> State.create 

    Assert.Equal(data, state.data)
    Assert.Equal(context, state.context)

[<Fact>]
let ``State.updateData updates a state's data`` () =
    let data = 0
    let context = "zero"
    let updater = (+) 1
    let updatedData = data |> updater

    let state = 
        (data, context) 
        |> State.create 
        |> State.updateData updater


    Assert.Equal(updatedData, state.data)
    Assert.Equal(context, state.context)

[<Fact>]
let ``State.updateContext updates a state's context`` () =
    let data = 0
    let context = "zero"
    let updater = (+) "one"
    let updatedContext = context |> updater

    let state = 
        (data, context) 
        |> State.create 
        |> State.updateContext updater

    Assert.Equal(data, state.data)
    Assert.Equal(updatedContext, state.context)
