module Utility.Tests.DirtyGrid

open Xunit
open Utility

[<Fact>]
let ``DirtyGrid.create makes an empty DirtyGrid`` () =
    let grid:DirtyGrid<unit> = DirtyGrid.create()

    Assert.True(grid.committed.IsEmpty)
    Assert.True(grid.pending.IsEmpty)

[<Fact>]
let ``DirtyGrid.set adds a pending cell to a DirtyGrid`` () =
    let location = (0,0)
    let grid:DirtyGrid<bool> = 
        DirtyGrid.create()
        |> DirtyGrid.set location true

    Assert.True(grid.committed.IsEmpty)
    Assert.Equal(1, grid.pending |> Map.count)

[<Fact>]
let ``DirtyGrid.put Some adds a pending cell to a DirtyGrid`` () =
    let location = (0,0)
    let grid:DirtyGrid<bool> = 
        DirtyGrid.create()
        |> DirtyGrid.put location (true |> Some)

    Assert.True(grid.committed.IsEmpty)
    Assert.Equal(1, grid.pending |> Map.count)

[<Fact>]
let ``DirtyGrid.set replaces a pending cell on a DirtyGrid`` () =
    let location = (0,0)
    let grid:DirtyGrid<bool> = 
        DirtyGrid.create()
        |> DirtyGrid.set location true
        |> DirtyGrid.set location false

    Assert.True(grid.committed.IsEmpty)
    Assert.Equal(1, grid.pending |> Map.count)

[<Fact>]
let ``DirtyGrid.reset adds a pending cell to a DirtyGrid`` () =
    let location = (0,0)
    let grid:DirtyGrid<bool> = 
        DirtyGrid.create()
        |> DirtyGrid.reset location

    Assert.True(grid.committed.IsEmpty)
    Assert.Equal(1, grid.pending |> Map.count)

[<Fact>]
let ``DirtyGrid.put None adds a pending cell to a DirtyGrid`` () =
    let location = (0,0)
    let grid:DirtyGrid<bool> = 
        DirtyGrid.create()
        |> DirtyGrid.put location None

    Assert.True(grid.committed.IsEmpty)
    Assert.Equal(1, grid.pending |> Map.count)

[<Fact>]
let ``DirtyGrid.reset replaces a pending cell on a DirtyGrid`` () =
    let location = (0,0)
    let grid:DirtyGrid<bool> = 
        DirtyGrid.create()
        |> DirtyGrid.reset location
        |> DirtyGrid.reset location

    Assert.True(grid.committed.IsEmpty)
    Assert.Equal(1, grid.pending |> Map.count)

[<Fact>]
let ``DirtyGrid.commit applies pending cells to committed cells on a DirtyGrid`` () =
    let location = (0,0)
    let grid:DirtyGrid<bool> = 
        DirtyGrid.create()
        |> DirtyGrid.put location (true |> Some)
        |> DirtyGrid.commit

    Assert.True(grid.pending.IsEmpty)
    Assert.Equal(1, grid.committed |> Map.count)

[<Fact>]
let ``DirtyGrid.putRect puts a rectangle of cells into the pending cells on a DirtyGrid`` () =
    let location = (0,0)
    let size = (2,3)
    let grid:DirtyGrid<bool> =
        DirtyGrid.create()
        |> DirtyGrid.putRect location size (true |> Some)

    Assert.True(grid.committed.IsEmpty)
    Assert.Equal(6, grid.pending |> Map.count)

[<Fact>]
let ``DirtyGrid.iter iterates through each of the cells committed on a DirtyGrid`` () =
    let location = (0,0)
    let size = (2,3)
    let grid:DirtyGrid<bool> =
        DirtyGrid.create()
        |> DirtyGrid.putRect location size (true |> Some)
        |> DirtyGrid.commit

    let mutable counter = 0

    grid
    |> DirtyGrid.iter
        (fun _ _ -> 
            counter<-counter+1)

    Assert.Equal(6, counter)

[<Fact>]
let ``DirtyGrid.fold accumulates through each of the cells committed on a DirtyGrid`` () =
    let location = (0,0)
    let size = (2,3)
    let grid:DirtyGrid<bool> =
        DirtyGrid.create()
        |> DirtyGrid.putRect location size (true |> Some)
        |> DirtyGrid.commit

    let counter = 
        grid
        |> DirtyGrid.fold 
            (fun a _ _ -> 
                a+1) 0

    Assert.Equal(6, counter)

[<Fact>]
let ``DirtyGrid.ofStringList maps character to values on a DirtyGrid`` () =
    let grid = 
        DirtyGrid.ofStringList 
            (fun c -> 
                if c = 'X' then (true |> Some) else None)
            [
                "X...X"
                ".X.X"
                "..X"
                ".X.X"
                "X...X"
            ]
    Assert.Equal(9, grid.committed |> Map.count)
