module Utility.Tests.Location

open Xunit
open Utility

[<Fact>]
let ``Location.move adds two locations together`` () =
    let x1 = 2
    let y1 = 3
    let x2 = 4
    let y2 = 5

    let result = (x1,y1) |> Location.move (x2,y2)

    Assert.Equal(x1+x2, result |> fst)
    Assert.Equal(y1+y2, result |> snd)

[<Fact>]
let ``Location.scale multiplies two locations together`` () =
    let x1 = 2
    let y1 = 3
    let x2 = 4
    let y2 = 5

    let result = (x1,y1) |> Location.scale (x2,y2)

    Assert.Equal(x1*x2, result |> fst)
    Assert.Equal(y1*y2, result |> snd)

[<Fact>]
let ``Location.toList makes a list containing the one location`` () =
    let x = 1
    let y = 2

    let result = (x,y) |> Location.toList

    Assert.Equal(1, result |> List.length)
    Assert.Equal(x, result |> List.head |> fst)
    Assert.Equal(y, result |> List.head |> snd)
