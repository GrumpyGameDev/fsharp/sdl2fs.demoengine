namespace Demo

module Runner =
    open Utility
    open Engine
    open DemoContent
    open SDL

    let run (screenScale: int) =
        use system = new System([ Flags.Video; Flags.Events; Flags.GameController ])
        (Engine.create Content.creatureifier Content.initialLocation Rooms.create Content.createAvatar Content.welcome
             4 |> Some,
         Context.create
             (GameConstants.title, (GameConstants.viewWidth * screenScale, GameConstants.viewHeight * screenScale),
              (GameConstants.viewWidth, GameConstants.viewHeight)))
        |> State.create
        |> EventPump.start Event.poll (Controller.onEvent) Controller.onIdle View.refresh
        |> Context.cleanUp
        |> ignore
