namespace Demo

module GameConstants =
    let viewWidth = 256
    let viewHeight = 192
    let defaultScreenScale = 3
    let title = "Renderer Demo"
    let patternWidth = 8
    let patternHeight = 8
    let patternColumns = viewWidth / patternWidth
    let patternRows = viewHeight / patternHeight
