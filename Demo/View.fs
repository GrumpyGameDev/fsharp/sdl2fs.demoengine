namespace Demo

type Sprite = Pattern * Hue option * Hue option

module View =
    open SDL
    open Utility
    open Engine
    open DemoContent

    let private drawPixel (hue: Hue) (location: Geometry.Point) (context: Context): unit =
        context.renderer |>* Render.setDrawColor hue.Color
        context.renderer |>* Render.drawPoint location

    let private drawPattern (location: Geometry.Point)
        (pattern: Pattern, foreground: Hue option, background: Hue option) (context: Context): unit =
        pattern.PixelMap
        |> DirtyGrid.iter (fun l v ->
            let p = (Geometry.Point.create l).MoveBy(location)
            match v, foreground, background with
            | true, Some hue, _ -> context |> drawPixel hue p
            | false, _, Some hue -> context |> drawPixel hue p
            | _ -> ())

    let private drawText (text: string, foreground: Hue option, background: Hue option) (location: Geometry.Point)
        (context: Context): unit =
        text.ToCharArray()
        |> Array.fold (fun (l: Geometry.Point) c ->
            match c |> Pattern.ofChar with
            | Some pattern ->
                context |> drawPattern (l) (pattern, foreground, background)
                l.MoveX(GameConstants.patternWidth)
            | None -> l) location
        |> ignore

    let private plot = Geometry.Point.create >> Geometry.Point.scaleBy Context.patternSize

    let private drawMenu (context: Context) (currentState: 'TState) (items: (string * (int * int) * 'TState) list): unit =
        items
        |> List.iter (fun (t, l, s) ->
            let foreground =
                if s = currentState then White |> Some else LightGray |> Some

            let background =
                if s = currentState then Hue.Blue |> Some else DarkGray |> Some

            context |> drawText (t, foreground, background) (l |> plot))


    let private drawMainMenuState (context: Context) (state: MainMenuState): unit =
        [ "  Resume  ", (11, 8), Resume
          "   New    ", (11, 9), New
          "   Load   ", (11, 10), Load
          "   Help   ", (11, 11), Help
          "  About   ", (11, 12), About
          " Options  ", (11, 13), Options
          "   Quit   ", (11, 14), Quit ]
        |> drawMenu context state

    let private drawDialogState (context: Context) (dialogState: DialogState): unit =
        [ for x in 0 .. (GameConstants.patternColumns - 1) do
            for y in 0 .. (GameConstants.patternRows - 1) do
                yield (x, y) ]
        |> List.iter (fun l -> drawPattern (l |> plot) (CharacterB1 |> Character, Black |> Some, None) context)

        match dialogState with
        | MainMenu state -> drawMainMenuState context state

    let private drawSprite (spriteFetcher: RoomCell<BaseTerrain, Creature, Room, Item, Toggle, Counter> -> Sprite option)
        (location: int * int) (cell: RoomCell<BaseTerrain, Creature, Room, Item, Toggle, Counter>): Context -> unit =
        cell
        |> spriteFetcher
        |> Option.map (drawPattern (location |> plot))
        |> Option.defaultValue ignore

    let private spriteForTerrain (counterConditionResolver: Counter -> CounterCondition -> bool)
        (toggleResolver: Toggle -> bool) (cell: RoomCell<BaseTerrain, Creature, Room, Item, Toggle, Counter>): Sprite option =
        cell.terrain
        |> Pattern.ofTerrain counterConditionResolver toggleResolver
        |> Option.map (fun pattern ->
            (pattern, cell.terrain |> ForegroundHue.ofTerrain counterConditionResolver toggleResolver,
             cell.terrain |> BackgroundHue.ofTerrain counterConditionResolver toggleResolver))

    let private spriteForCreature (cell: RoomCell<BaseTerrain, Creature, Room, Item, Toggle, Counter>): Sprite option =
        cell.creature
        |> Option.bind Pattern.ofCreature
        |> Option.map (fun pattern ->
            (pattern, cell.creature.Value |> ForegroundHue.ofCreature, cell.creature.Value |> BackgroundHue.ofCreature))

    let private spriteForItem (cell: RoomCell<BaseTerrain, Creature, Room, Item, Toggle, Counter>): Sprite option =
        cell.item
        |> Option.bind Pattern.ofItem
        |> Option.map (fun pattern ->
            (pattern, cell.item.Value |> ForegroundHue.ofItem, cell.item.Value |> BackgroundHue.ofItem))

    let private redrawEngineState (context: Context)
        (engine: Engine<BaseTerrain, Creature, Avatar, Room, Item, Toggle, DialogState, Counter>): unit =
        let currentRoom = engine.roomGrids.[engine |> Engine.getRoom] |> snd

        currentRoom
        |> DirtyGrid.iter (fun l c ->
            context |> drawSprite (spriteForTerrain engine.TestCounterCondition engine.toggles.Contains) l c

            context |> drawSprite spriteForCreature l c

            context |> drawSprite spriteForItem l c)

        engine
        |> Engine.getInteractLocation Content.avatarifier Avatar.getFacing
        |> Option.iter (fun l ->
            context
            |> drawPattern ((l |> Geometry.Point.create).ScaleBy Context.patternSize) (Selector, White |> Some, None))

        engine.messages
        |> List.fold (fun (y: int) (i: MessageLine) ->
            context
            |> drawText
                (i.text, (i.messageLineType |> ForegroundHue.ofMessageType),
                 (i.messageLineType |> BackgroundHue.ofMessageType)) ((0, y) |> plot)
            (y + 1)) ContentConstants.roomRows
        |> ignore

        engine.dialogState |> Option.iter (drawDialogState context)


    let internal refresh (state: State<Engine<BaseTerrain, Creature, Avatar, Room, Item, Toggle, DialogState, Counter> option, Context>): unit =
        state.context.renderer |>* Render.setDrawColor Black.Color

        state.context.renderer |>* Render.clear

        state.context.renderer |>* Render.setDrawColor White.Color

        state.data |> Option.iter (redrawEngineState state.context)

        state.context.renderer |> Render.present
