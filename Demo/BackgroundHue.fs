namespace Demo

module BackgroundHue =
    open Engine
    open DemoContent

    let rec internal ofTerrain (counterConditionResolver: Counter -> CounterCondition -> bool)
            (toggleResolver: Toggle -> bool) (terrain: Terrain<BaseTerrain, Room, Toggle, Counter>): Hue option =
        match terrain with
        | CounterCondition(counter, condition, whenFalse, whenTrue) ->
            if (counter, condition) ||> counterConditionResolver
            then whenTrue |> ofTerrain counterConditionResolver toggleResolver
            else whenFalse |> ofTerrain counterConditionResolver toggleResolver
        | Toggled(toggle, untoggled, toggled) ->
            if toggle |> toggleResolver
            then toggled |> ofTerrain counterConditionResolver toggleResolver
            else untoggled |> ofTerrain counterConditionResolver toggleResolver
        | Trigger(_, t) -> t |> ofTerrain counterConditionResolver toggleResolver
        | Obstacle t -> t |> ofTerrain counterConditionResolver toggleResolver
        | Base Floor -> Black |> Some
        | Base Wall -> DarkGray |> Some
        | Base Grass -> Black |> Some
        | Base Tree -> Black |> Some
        | Base Road -> DarkGray |> Some
        | Base Sign -> Black |> Some
        | Base (Door _) -> Black |> Some

    let internal ofCreature (creature: Creature): Hue option =
        match creature with
        | Tagon _ -> None

    let internal ofItem (item: Item): Hue option =
        match item with
        | Knife -> None

    let internal ofMessageType (messageType: MessageLineType): Hue option =
        match messageType with
        | Info -> None
