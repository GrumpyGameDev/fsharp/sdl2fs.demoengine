namespace Demo

open SDL

type Hue =
    | Black
    | Blue
    | Green
    | Cyan
    | Red
    | Magenta
    | Brown
    | LightGray
    | DarkGray
    | LightBlue
    | LightGreen
    | LightCyan
    | LightRed
    | LightMagenta
    | Yellow
    | White
    member x.Color =
        match x with
        | Black -> Pixel.Color.fromRGBA (0x00uy, 0x00uy, 0x00uy, 0xFFuy)
        | Blue -> Pixel.Color.fromRGBA (0x00uy, 0x00uy, 0xAAuy, 0xFFuy)
        | Green -> Pixel.Color.fromRGBA (0x00uy, 0xAAuy, 0x00uy, 0xFFuy)
        | Cyan -> Pixel.Color.fromRGBA (0x00uy, 0xAAuy, 0xAAuy, 0xFFuy)
        | Red -> Pixel.Color.fromRGBA (0xAAuy, 0x00uy, 0x00uy, 0xFFuy)
        | Magenta -> Pixel.Color.fromRGBA (0xAAuy, 0x00uy, 0xAAuy, 0xFFuy)
        | Brown -> Pixel.Color.fromRGBA (0xAAuy, 0x55uy, 0x00uy, 0xFFuy)
        | LightGray -> Pixel.Color.fromRGBA (0xAAuy, 0xAAuy, 0xAAuy, 0xFFuy)
        | DarkGray -> Pixel.Color.fromRGBA (0x55uy, 0x55uy, 0x55uy, 0xFFuy)
        | LightBlue -> Pixel.Color.fromRGBA (0x55uy, 0x55uy, 0xFFuy, 0xFFuy)
        | LightGreen -> Pixel.Color.fromRGBA (0x55uy, 0xFFuy, 0x55uy, 0xFFuy)
        | LightCyan -> Pixel.Color.fromRGBA (0x55uy, 0xFFuy, 0xFFuy, 0xFFuy)
        | LightRed -> Pixel.Color.fromRGBA (0xFFuy, 0x55uy, 0x55uy, 0xFFuy)
        | LightMagenta -> Pixel.Color.fromRGBA (0xFFuy, 0x55uy, 0xFFuy, 0xFFuy)
        | Yellow -> Pixel.Color.fromRGBA (0xFFuy, 0xFFuy, 0x55uy, 0xFFuy)
        | White -> Pixel.Color.fromRGBA (0xFFuy, 0xFFuy, 0xFFuy, 0xFFuy)
