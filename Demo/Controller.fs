namespace Demo


module Controller =
    open Utility
    open Engine
    open DemoContent
    open SDL

    let private doCommand =
        Engine.doCommand Content.counterClamper Content.dialogHandler Content.creatureifier Content.avatarifier
            Avatar.getFacing Avatar.setFacing Content.defaultCommandHandler

    let private keyMap =
        [ Keyboard.ScanCode.Up, Up
          Keyboard.ScanCode.Left, Left
          Keyboard.ScanCode.Down, Down
          Keyboard.ScanCode.Right, Right
          Keyboard.ScanCode.A, Blue
          Keyboard.ScanCode.S, Yellow
          Keyboard.ScanCode.Z, Green
          Keyboard.ScanCode.X, Red
          Keyboard.ScanCode.Escape, Back
          Keyboard.ScanCode.Return, Command.Start
          Keyboard.ScanCode.RightBracket, Next
          Keyboard.ScanCode.LeftBracket, Previous ]
        |> Map.ofList

    let private handleKeyDownEvent (keyEvent: Event.KeyboardEvent)
        (state: State<Engine<BaseTerrain, Creature, Avatar, Room, Item, Toggle, DialogState, Counter> option, Context>): EventResult<Engine<BaseTerrain, Creature, Avatar, Room, Item, Toggle, DialogState, Counter> option, Context> =
        match keyMap |> Map.tryFind keyEvent.Keysym.Scancode with
        | Some command ->
            state
            |> State.updateData (Option.bind (doCommand command))
            |> Continue
        | _ -> state |> Continue

    let private handleControllerDeviceAdded (deviceEvent: Event.ControllerDeviceEvent)
        (state: State<Engine<BaseTerrain, Creature, Avatar, Room, Item, Toggle, DialogState, Counter> option, Context>): EventResult<Engine<BaseTerrain, Creature, Avatar, Room, Item, Toggle, DialogState, Counter> option, Context> =
        state
        |> State.updateContext (Context.addController deviceEvent.Which)
        |> Continue

    let private handleControllerDeviceRemoved (deviceEvent: Event.ControllerDeviceEvent)
        (state: State<Engine<BaseTerrain, Creature, Avatar, Room, Item, Toggle, DialogState, Counter> option, Context>): EventResult<Engine<BaseTerrain, Creature, Avatar, Room, Item, Toggle, DialogState, Counter> option, Context> =
        state
        |> State.updateContext (Context.removeController deviceEvent.Which)
        |> Continue

    let private buttonMap =
        [ GameController.Button.A, Green
          GameController.Button.B, Red
          GameController.Button.X, Blue
          GameController.Button.Y, Yellow
          GameController.Button.DPadUp, Up
          GameController.Button.DPadDown, Down
          GameController.Button.DPadLeft, Left
          GameController.Button.DPadRight, Right
          GameController.Button.Back, Back
          GameController.Button.Start, Command.Start
          GameController.Button.LeftShoulder, Previous
          GameController.Button.RightShoulder, Next ]
        |> Map.ofList

    let private handleControllerButtonDown (buttonEvent: Event.ControllerButtonEvent)
        (state: State<Engine<BaseTerrain, Creature, Avatar, Room, Item, Toggle, DialogState, Counter> option, Context>): EventResult<Engine<BaseTerrain, Creature, Avatar, Room, Item, Toggle, DialogState, Counter> option, Context> =
        match buttonMap |> Map.tryFind buttonEvent.Button with
        | Some command ->
            state
            |> State.updateData (Option.bind (doCommand command))
            |> Continue
        | None -> state |> Continue

    let private handleEvent (event: Event.Event)
        (state: State<Engine<BaseTerrain, Creature, Avatar, Room, Item, Toggle, DialogState, Counter> option, Context>): EventResult<Engine<BaseTerrain, Creature, Avatar, Room, Item, Toggle, DialogState, Counter> option, Context> =
        match event with
        | Event.ControllerButtonDown cbd -> state |> handleControllerButtonDown cbd
        | Event.ControllerDeviceAdded cda -> state |> handleControllerDeviceAdded cda
        | Event.ControllerDeviceRemoved cdr -> state |> handleControllerDeviceRemoved cdr
        | Event.KeyDown ke -> state |> handleKeyDownEvent ke
        | Event.Quit _ -> state.context |> Stop
        | _ -> state |> Continue

    let private preEvent (state: State<Engine<BaseTerrain, Creature, Avatar, Room, Item, Toggle, DialogState, Counter> option, Context>): State<Engine<BaseTerrain, Creature, Avatar, Room, Item, Toggle, DialogState, Counter> option, Context> =
        state

    let private postEvent (result: EventResult<Engine<BaseTerrain, Creature, Avatar, Room, Item, Toggle, DialogState, Counter> option, Context>): EventResult<Engine<BaseTerrain, Creature, Avatar, Room, Item, Toggle, DialogState, Counter> option, Context> =
        match result with
        | Continue state ->
            match state.data with
            | Some _ -> state |> Continue
            | None -> state.context |> Stop
        | x -> x

    let internal onEvent (event: Event.Event) =
        preEvent
        >> handleEvent event
        >> postEvent

    let internal onIdle = id
