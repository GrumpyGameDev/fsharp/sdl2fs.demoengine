namespace Demo

module ForegroundHue =
    open Engine
    open DemoContent

    let rec internal ofTerrain 
            (counterConditionResolver: Counter -> CounterCondition -> bool)
            (toggleResolver: Toggle -> bool) 
            (terrain: Terrain<BaseTerrain, Room, Toggle, Counter>)
            : Hue option =
        match terrain with
        | CounterCondition(counter, condition, whenFalse, whenTrue) ->
            if (counter, condition) ||> counterConditionResolver
            then whenTrue |> ofTerrain counterConditionResolver toggleResolver
            else whenFalse |> ofTerrain counterConditionResolver toggleResolver

        | Toggled(toggle, untoggled, toggled) ->
            if toggle |> toggleResolver
            then toggled |> ofTerrain counterConditionResolver toggleResolver
            else untoggled |> ofTerrain counterConditionResolver toggleResolver

        | Trigger(_, t) -> t |> ofTerrain counterConditionResolver toggleResolver

        | Obstacle t -> t |> ofTerrain counterConditionResolver toggleResolver

        | Base Floor -> DarkGray |> Some

        | Base Wall -> Hue.Red |> Some

        | Base Grass -> Hue.Green |> Some

        | Base Tree -> Hue.Green |> Some

        | Base Road -> Black |> Some

        | Base (Door _)
        | Base Sign -> Brown |> Some

    let internal ofCreature (creature: Creature): Hue option =
        match creature with
        | Tagon _ -> Magenta |> Some

    let internal ofItem (item: Item): Hue option =
        match item with
        | Knife -> LightGray |> Some

    let internal ofMessageType (messageType: MessageLineType): Hue option =
        match messageType with
        | Info -> White |> Some
