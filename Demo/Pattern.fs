namespace Demo

open Utility
open Engine
open DemoContent

type Pattern =
    | Character of CharacterPattern

    | Acorn       
    | Axe         
    | Berries     
    | BerrySeed   
    | Bone        
    | Box         
    | Bush        
    | Cursor      
    | DeadRabbit  
    | DeadTagon   
    | Door        
    | Empty       
    | Energy      
    | Eye         
    | Field       
    | Fish        
    | Fire        
    | Furnace     
    | Hammer      
    | Hoe         
    | Hole        
    | House       
    | Ingot       
    | Key         
    | Knife       
    | LeftHand    
    | Log         
    | Maze0       
    | Maze1       
    | Maze2       
    | Maze3       
    | Maze4       
    | Maze5       
    | Maze6       
    | Maze7       
    | Maze8       
    | Maze9       
    | Maze10      
    | Maze11      
    | Maze12      
    | Maze13      
    | Maze14      
    | Maze15      
    | Meat        
    | Mouth       
    | Mushroom    
    | OpenDoor    
    | Pelt        
    | PickAxe     
    | Pile        
    | Pin         
    | Rabbit      
    | RightHand   
    | Rock        
    | Sapling     
    | Selector    
    | Sign        
    | Shovel      
    | Solid       
    | Spear       
    | Stomach     
    | Stone       
    | Tagon       
    | Trap        
    | Tree        
    | UIDown      
    | UIDownDown  
    | UILeft      
    | UILeftDown  
    | UIRight     
    | UIRightDown 
    | UIUp        
    | UIUpDown    
    | Wall        
    | Workbench   
    member x.PixelMap : DirtyGrid<bool> =
        match x with
        | Character c -> c.PixelMap

        | Acorn       -> [0x00uy; 0x00uy; 0x10uy; 0x08uy; 0x3Euy; 0x1cuy; 0x08uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Axe         -> [0x12uy; 0x16uy; 0x1Euy; 0x1Euy; 0x16uy; 0x12uy; 0x10uy; 0x10uy] |> DirtyGrid.ofByteList Some
        | Berries     -> [0x00uy; 0x00uy; 0x10uy; 0x04uy; 0x20uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | BerrySeed   -> [0x00uy; 0x00uy; 0x00uy; 0x08uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Bone        -> [0x00uy; 0x30uy; 0x70uy; 0x78uy; 0x1cuy; 0x0fuy; 0x07uy; 0x06uy] |> DirtyGrid.ofByteList Some
        | Box         -> [0xFFuy; 0x81uy; 0x81uy; 0x81uy; 0x81uy; 0x81uy; 0x81uy; 0xFFuy] |> DirtyGrid.ofByteList Some
        | Bush        -> [0x00uy; 0x3cuy; 0x6euy; 0xfbuy; 0xdfuy; 0x7Euy; 0x18uy; 0x18uy] |> DirtyGrid.ofByteList Some
        | Cursor      -> [0x00uy; 0x00uy; 0x00uy; 0x00uy; 0x08uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | DeadRabbit  -> [0x00uy; 0x66uy; 0xfcuy; 0x7fuy; 0x07uy; 0x04uy; 0x04uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | DeadTagon   -> [0x14uy; 0x14uy; 0x1cuy; 0x5duy; 0x3Euy; 0x08uy; 0x1cuy; 0x1cuy] |> DirtyGrid.ofByteList Some
        | Door        -> [0x18uy; 0x3cuy; 0x7Euy; 0x7Euy; 0x7Euy; 0x6euy; 0x7Euy; 0x7Euy] |> DirtyGrid.ofByteList Some
        | Empty       -> [0x00uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Energy      -> [0x78uy; 0x3cuy; 0x1Euy; 0x3fuy; 0x18uy; 0x0cuy; 0x04uy; 0x02uy] |> DirtyGrid.ofByteList Some
        | Eye         -> [0x00uy; 0x1cuy; 0x36uy; 0x6buy; 0x36uy; 0x1cuy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Field       -> [0x00uy; 0x22uy; 0x00uy; 0x88uy; 0x00uy; 0x22uy; 0x00uy; 0x88uy] |> DirtyGrid.ofByteList Some
        | Fish        -> [0x00uy; 0x00uy; 0x5euy; 0x7fuy; 0x5euy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Fire        -> [0x10uy; 0x18uy; 0x0cuy; 0x1cuy; 0x3Euy; 0x3Euy; 0x1cuy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Furnace     -> [0x3cuy; 0x7euy; 0xe7uy; 0xc3uy; 0xc3uy; 0xc3uy; 0xffuy; 0xffuy] |> DirtyGrid.ofByteList Some
        | Hammer      -> [0x3Euy; 0x3Euy; 0x08uy; 0x08uy; 0x08uy; 0x08uy; 0x08uy; 0x08uy] |> DirtyGrid.ofByteList Some
        | Hoe         -> [0x0euy; 0x08uy; 0x08uy; 0x08uy; 0x08uy; 0x08uy; 0x08uy; 0x08uy] |> DirtyGrid.ofByteList Some
        | Hole        -> [0x00uy; 0x00uy; 0x18uy; 0x3cuy; 0x3cuy; 0x18uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | House       -> [0x08uy; 0x1cuy; 0x3Euy; 0x7fuy; 0x3Euy; 0x36uy; 0x36uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Ingot       -> [0x00uy; 0x00uy; 0x3cuy; 0x7euy; 0xffuy; 0xffuy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Key         -> [0x00uy; 0x1cuy; 0x14uy; 0x1cuy; 0x08uy; 0x0cuy; 0x0cuy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Knife       -> [0x00uy; 0x10uy; 0x18uy; 0x18uy; 0x18uy; 0x10uy; 0x10uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | LeftHand    -> [0x38uy; 0x3cuy; 0x3cuy; 0xbcuy; 0xfcuy; 0x7cuy; 0x3cuy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Log         -> [0x00uy; 0x00uy; 0x7fuy; 0x3Euy; 0x7fuy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Maze0       -> [0x00uy; 0x00uy; 0x00uy; 0x18uy; 0x18uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Maze1       -> [0x18uy; 0x18uy; 0x18uy; 0x18uy; 0x18uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Maze2       -> [0x00uy; 0x00uy; 0x00uy; 0xF8uy; 0xF8uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Maze3       -> [0x18uy; 0x18uy; 0x18uy; 0xF8uy; 0xF8uy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Maze4       -> [0x00uy; 0x00uy; 0x00uy; 0x18uy; 0x18uy; 0x18uy; 0x18uy; 0x18uy] |> DirtyGrid.ofByteList Some
        | Maze5       -> [0x18uy; 0x18uy; 0x18uy; 0x18uy; 0x18uy; 0x18uy; 0x18uy; 0x18uy] |> DirtyGrid.ofByteList Some
        | Maze6       -> [0x00uy; 0x00uy; 0x00uy; 0xF8uy; 0xF8uy; 0x18uy; 0x18uy; 0x18uy] |> DirtyGrid.ofByteList Some
        | Maze7       -> [0x18uy; 0x18uy; 0x18uy; 0xF8uy; 0xF8uy; 0x18uy; 0x18uy; 0x18uy] |> DirtyGrid.ofByteList Some
        | Maze8       -> [0x00uy; 0x00uy; 0x00uy; 0x1Fuy; 0x1Fuy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Maze9       -> [0x18uy; 0x18uy; 0x18uy; 0x1Fuy; 0x1Fuy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Maze10      -> [0x00uy; 0x00uy; 0x00uy; 0xFFuy; 0xFFuy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Maze11      -> [0x18uy; 0x18uy; 0x18uy; 0xFFuy; 0xFFuy; 0x00uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Maze12      -> [0x00uy; 0x00uy; 0x00uy; 0x1Fuy; 0x1Fuy; 0x18uy; 0x18uy; 0x18uy] |> DirtyGrid.ofByteList Some
        | Maze13      -> [0x18uy; 0x18uy; 0x18uy; 0x1Fuy; 0x1Fuy; 0x18uy; 0x18uy; 0x18uy] |> DirtyGrid.ofByteList Some
        | Maze14      -> [0x00uy; 0x00uy; 0x00uy; 0xFFuy; 0xFFuy; 0x18uy; 0x18uy; 0x18uy] |> DirtyGrid.ofByteList Some
        | Maze15      -> [0x18uy; 0x18uy; 0x18uy; 0xFFuy; 0xFFuy; 0x18uy; 0x18uy; 0x18uy] |> DirtyGrid.ofByteList Some
        | Meat        -> [0x00uy; 0x18uy; 0x3cuy; 0x3cuy; 0x3cuy; 0x3cuy; 0x18uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Mouth       -> [0x00uy; 0x00uy; 0x00uy; 0x41uy; 0x3euy; 0x1cuy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Mushroom    -> [0x3euy; 0x7fuy; 0x7fuy; 0x7fuy; 0x00uy; 0x1cuy; 0x1cuy; 0x00uy] |> DirtyGrid.ofByteList Some
        | OpenDoor    -> [0x18uy; 0x24uy; 0x42uy; 0x42uy; 0x42uy; 0x42uy; 0x42uy; 0x7Euy] |> DirtyGrid.ofByteList Some
        | Pelt        -> [0x18uy; 0x7Euy; 0x3cuy; 0x3cuy; 0x3cuy; 0x3cuy; 0x7Euy; 0x42uy] |> DirtyGrid.ofByteList Some
        | PickAxe     -> [0x3Euy; 0x49uy; 0x08uy; 0x08uy; 0x08uy; 0x08uy; 0x08uy; 0x08uy] |> DirtyGrid.ofByteList Some
        | Pile        -> [0x00uy; 0x00uy; 0x00uy; 0x14uy; 0x2auy; 0x55uy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Pin         -> [0x60uy; 0x70uy; 0x3Euy; 0x1Euy; 0x1cuy; 0x1auy; 0x01uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Rabbit      -> [0x00uy; 0x04uy; 0x04uy; 0x07uy; 0x7fuy; 0xfcuy; 0x66uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | RightHand   -> [0x1cuy; 0x3cuy; 0x3cuy; 0x3duy; 0x3fuy; 0x3Euy; 0x3cuy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Rock        -> [0x00uy; 0x00uy; 0x00uy; 0x34uy; 0x7Euy; 0x7Euy; 0x3Euy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Sapling     -> [0x00uy; 0x08uy; 0x08uy; 0x1cuy; 0x08uy; 0x3Euy; 0x08uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Selector    -> [0xc3uy; 0x81uy; 0x00uy; 0x00uy; 0x00uy; 0x00uy; 0x81uy; 0xc3uy] |> DirtyGrid.ofByteList Some
        | Sign        -> [0x7fuy; 0x63uy; 0x6fuy; 0x67uy; 0x7fuy; 0x77uy; 0x7fuy; 0x08uy] |> DirtyGrid.ofByteList Some
        | Shovel      -> [0x08uy; 0x08uy; 0x08uy; 0x08uy; 0x08uy; 0x3Euy; 0x3Euy; 0x1cuy] |> DirtyGrid.ofByteList Some
        | Solid       -> [0xFFuy; 0xFFuy; 0xFFuy; 0xFFuy; 0xFFuy; 0xFFuy; 0xFFuy; 0xFFuy] |> DirtyGrid.ofByteList Some
        | Spear       -> [0x70uy; 0x60uy; 0x50uy; 0x08uy; 0x04uy; 0x02uy; 0x01uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Stomach     -> [0x02uy; 0x3euy; 0x78uy; 0x78uy; 0x3euy; 0x3fuy; 0x01uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Stone       -> [0x00uy; 0x00uy; 0x38uy; 0x3cuy; 0x3cuy; 0x1cuy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Tagon       -> [0x1Cuy; 0x1Cuy; 0x08uy; 0x3Euy; 0x5Duy; 0x1Cuy; 0x14uy; 0x14uy] |> DirtyGrid.ofByteList Some
        | Trap        -> [0x00uy; 0x00uy; 0x55uy; 0x55uy; 0x55uy; 0x7fuy; 0x00uy; 0x00uy] |> DirtyGrid.ofByteList Some
        | Tree        -> [0x10uy; 0x10uy; 0x38uy; 0x38uy; 0x7Cuy; 0x7Cuy; 0xFEuy; 0x10uy] |> DirtyGrid.ofByteList Some
        | UIDown      -> [0x3Euy; 0x41uy; 0xc1uy; 0xd5uy; 0xc9uy; 0xc1uy; 0xfeuy; 0x7cuy] |> DirtyGrid.ofByteList Some
        | UIDownDown  -> [0x00uy; 0x7cuy; 0x82uy; 0x82uy; 0xaauy; 0x92uy; 0x82uy; 0x7cuy] |> DirtyGrid.ofByteList Some
        | UILeft      -> [0x3Euy; 0x41uy; 0xc9uy; 0xc5uy; 0xc9uy; 0xc1uy; 0xfeuy; 0x7cuy] |> DirtyGrid.ofByteList Some
        | UILeftDown  -> [0x00uy; 0x7cuy; 0x82uy; 0x92uy; 0x8auy; 0x92uy; 0x82uy; 0x7cuy] |> DirtyGrid.ofByteList Some
        | UIRight     -> [0x3Euy; 0x41uy; 0xc9uy; 0xd1uy; 0xc9uy; 0xc1uy; 0xfeuy; 0x7cuy] |> DirtyGrid.ofByteList Some
        | UIRightDown -> [0x00uy; 0x7cuy; 0x82uy; 0x92uy; 0xa2uy; 0x92uy; 0x82uy; 0x7cuy] |> DirtyGrid.ofByteList Some
        | UIUp        -> [0x3Euy; 0x41uy; 0xc9uy; 0xd5uy; 0xc1uy; 0xc1uy; 0xfeuy; 0x7cuy] |> DirtyGrid.ofByteList Some
        | UIUpDown    -> [0x00uy; 0x7cuy; 0x82uy; 0x92uy; 0xaauy; 0x82uy; 0x82uy; 0x7cuy] |> DirtyGrid.ofByteList Some
        | Wall        -> [0x00uy; 0xfeuy; 0xfeuy; 0xfeuy; 0x00uy; 0xefuy; 0xefuy; 0xefuy] |> DirtyGrid.ofByteList Some
        | Workbench   -> [0x00uy; 0x00uy; 0x00uy; 0xffuy; 0xffuy; 0x66uy; 0x66uy; 0x66uy] |> DirtyGrid.ofByteList Some

module Pattern =
    let ofChar (c:char) : Pattern option =
        c
        |> CharacterPattern.ofChar
        |> Option.map Character
        
    let rec internal ofTerrain (counterConditionResolver:Counter->CounterCondition->bool) (toggleResolver:Toggle->bool) (terrain:Terrain<BaseTerrain, Room, Toggle, Counter>) : Pattern option =
        match terrain with
        | CounterCondition (counter, condition, whenFalse, whenTrue) ->
            if (counter, condition) ||> counterConditionResolver then
                whenTrue |> ofTerrain counterConditionResolver toggleResolver
            else
                whenFalse |> ofTerrain counterConditionResolver toggleResolver

        | Toggled (toggle, untoggled, toggled) ->
            if toggle |> toggleResolver then
                toggled |> ofTerrain counterConditionResolver toggleResolver
            else 
                untoggled |> ofTerrain counterConditionResolver toggleResolver
        | Trigger (_,t) ->
            t |> ofTerrain counterConditionResolver toggleResolver
        | Obstacle t ->
            t |> ofTerrain counterConditionResolver toggleResolver
        | Base Floor ->
            Field |> Some
        | Base BaseTerrain.Wall ->
            Wall |> Some
        | Base BaseTerrain.Tree ->
            Tree |> Some
        | Base Road
        | Base Grass ->
            Field |> Some
        | Base (BaseTerrain.Sign) ->
            Sign |> Some
        | Base (BaseTerrain.Door false) ->
            OpenDoor |> Some
        | Base (BaseTerrain.Door true) ->
            Door |> Some
        

    let internal ofCreature (creature:Creature) : Pattern option =
        match creature with
        | Creature.Tagon _ ->
            Tagon
            |> Some

    let internal ofItem (item:Item) : Pattern option =
        match item with
        | Item.Knife ->
            Knife
            |> Some

