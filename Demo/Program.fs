﻿open Demo
open DemoContent

[<EntryPoint>]
let main argv =
    printfn "Please don't close this window. If you do, the game will die."
    if argv.Length > 0 then
        match argv.[0] |> System.Int32.TryParse with
        | true, scale when scale >=1 ->
            scale
        | _ ->
            GameConstants.defaultScreenScale
    else
        GameConstants.defaultScreenScale
    |> Runner.run
    0
