namespace Demo

open SDL

type Context =
    { window: Window.Window
      renderer: Render.Renderer
      controllers: Map<int, GameController.Controller> }
    member x.CleanUp(): Context =
        x.controllers |> Map.iter (fun _ v -> v.Destroy())
        x.renderer.Destroy()
        x.window.Destroy()
        { renderer = Render.Renderer.Invalid
          window = Window.Window.Invalid
          controllers = Map.empty }

module Context =
    let addController (which: int) (context: Context): Context =
        { context with controllers = context.controllers |> Map.add which (which |> GameController.create) }

    let removeController (which: int) (context: Context): Context =
        context.controllers
        |> Map.tryFind which
        |> Option.iter (fun c -> c.Destroy())
        { context with controllers = context.controllers |> Map.remove which }

    let create (title: string, (width: int, height: int), (logicalWidth: int, logicalHeight: int)): Context =
        let window = Window.create (title, Window.Position.Centered, width, height, Window.Flags.None)

        let renderer = Render.create window None Render.Flags.Accelerated

        renderer |>* Render.setLogicalSize (logicalWidth, logicalHeight)

        { window = window
          renderer = renderer
          controllers = Map.empty }

    let cleanUp (context: Context): Context = context.CleanUp()

    let patternSize = Geometry.Point.create (GameConstants.patternWidth, GameConstants.patternHeight)
